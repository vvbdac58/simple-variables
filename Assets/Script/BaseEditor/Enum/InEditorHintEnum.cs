﻿namespace SimpleVariables.ReflectionEditor
{
    public enum InEditorHintEnum
    {
        SerializedProperty,
        Bool,
        String,
        Int,
        Float,
        Vector3,
        Vector4,
        Quaternion,
        Color,
        Object,
    }
}