using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Reflection;
using System.Linq;

namespace SimpleVariables.ReflectionEditor
{
    /// <summary>
    /// Base editor class that draw members of class
    /// that have InEditorAttribute.
    /// </summary>
    public class BaseEditor : Editor
    {
        /// <summary>
        /// Stored members.
        /// </summary>
        protected MemberInfo[] members;
        /// <summary>
        /// That points out members it draws
        /// with base classes' members.
        /// </summary>
        protected readonly BindingFlags flags =
            BindingFlags.Public | BindingFlags.Instance |
            BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.FlattenHierarchy;
        public void OnEnable()
        {
            Type t = serializedObject.targetObject.GetType();
            members = t.GetMembers(flags)
                .Where(m => m.GetCustomAttribute<InEditorAttribute>() is object)
                .ToArray();
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            foreach (var m in members)
            {
                var prop = serializedObject.FindProperty(m.Name);
                if (prop is object && !prop.Equals(null))
                    EditorGUILayout.PropertyField(prop);
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}