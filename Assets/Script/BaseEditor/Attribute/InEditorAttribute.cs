﻿using System;
using UnityEngine;

namespace SimpleVariables.ReflectionEditor
{
    public class InEditorAttribute : PropertyAttribute
    {
        public InEditorHintEnum Hint;
        public Type TargetType;
        public InEditorAttribute(InEditorHintEnum hint, Type targetType)
        {
            Hint = hint;
            TargetType = targetType;
        }
        public InEditorAttribute(InEditorHintEnum hint)
        {
            Hint = hint;
        }
    }
}