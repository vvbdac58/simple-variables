﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace SimpleVariables
{
    public class CustomTextAreaData
    {
        public int First = 0;
        public int Last = 0;
        public string Text = string.Empty;
        public CustomTextAreaMovingDirectionEnum Direction = CustomTextAreaMovingDirectionEnum.First;

        public GUIStyle Style;

        public GUIContent Content
        {
            get => new GUIContent(Text);
        }
        public int Head
        {
            get
            {
                return First < Last ? First : Last;
            }
        }
        public int Length
        {
            get
            {
                return Mathf.Abs(Last - First);
            }
        }

        public CustomTextAreaData(string text, GUIStyle style)
        {
            Text = text;
            Style = style;
        }

        public void SelectAll()
        {
            First = 0;
            Last = Text.Length;
        }
        public void Insert(string inserted)
        {
            if (Length > 0)
            {
                Text = Text.Remove(Head, Length);
                Text = Text.Insert(Head, inserted);
                Last = First = Head + 1;
            }
            else
            {
                Text = Text.Insert(Head, inserted);
                Last = First += 1;
            }
        }
        public void Delete()
        {
            if (Text.Length > 0)
                Text = Text.Remove(Head, Length);
            else if (First < Text.Length)
                Text = Text.Remove(First, 1);
            Last = First = Head;
        }
        public void Backspace()
        {
            if (Length > 0)
            {
                Text = Text.Remove(Head, Length);
                Last = First = Head;
            }
            else
            {
                var to = Head - 1;
                if (to >= 0 && to < Text.Length)
                {
                    Text = Text.Remove(to, 1);
                    First = Last = to;
                }
            }
        }

        public void MoveLeft()
        {
            if (Length == 0)
                Direction = CustomTextAreaMovingDirectionEnum.First;

            int cursor = Direction == CustomTextAreaMovingDirectionEnum.First ? First : Last;
            int to;
            if (Event.current.control)
            {
                var strings = ExpressionUtility.ToStrings(Text);

                int i = 0;
                to = 0;
                for (; i < strings.Count; i++)
                {
                    if (to + strings[i].Length >= cursor)
                        break;
                    else
                        to += strings[i].Length;
                }
            }
            else
            {
                to = cursor - 1 >= 0 ? cursor - 1 : 0;
            }
            switch (Direction)
            {
                case CustomTextAreaMovingDirectionEnum.First:
                    if (Event.current.shift)
                        First = to;
                    else
                        Last = First = to;
                    break;
                case CustomTextAreaMovingDirectionEnum.Last:
                    if (Event.current.shift)
                        Last = to;
                    else
                        Last = First = to;
                    break;
            }
        }
        public void MoveRight()
        {
            if (Length == 0)
                Direction = CustomTextAreaMovingDirectionEnum.Last;

            int cursor = Direction == CustomTextAreaMovingDirectionEnum.First ? First : Last;
            int to = 0;
            if (Event.current.control)
            {
                var strings = ExpressionUtility.ToStrings(Text);

                int i = 0;
                to = 0;
                for (; i < strings.Count; i++)
                {
                    to += strings[i].Length;
                    if (to > cursor)
                        break;
                }
            }
            else
            {
                to = cursor + 1 <= Text.Length ? cursor + 1 : Text.Length;
            }
            switch (Direction)
            {
                case CustomTextAreaMovingDirectionEnum.First:
                    if (Event.current.shift)
                        First = to;
                    else
                        First = Last = to;
                    break;
                case CustomTextAreaMovingDirectionEnum.Last:
                    if (Event.current.shift)
                        Last = to;
                    else
                        First = Last = to;
                    break;
            }
        }
        public void MoveUp(Rect position)
        {
            if (Length == 0)
                Direction = CustomTextAreaMovingDirectionEnum.First;

            var content = new GUIContent(Text);
            int cursor = Direction == CustomTextAreaMovingDirectionEnum.First ? First : Last;
            var pos = Style.GetCursorPixelPosition(position, content, cursor);
            pos.y -= Style.lineHeight * 0.5f;
            var to = Style.GetCursorStringIndex(position, content, pos);

            switch (Direction) 
            {
                case CustomTextAreaMovingDirectionEnum.First:
                    if (Event.current.shift)
                        First = to;
                    else
                        Last = First = to;
                    break;
                case CustomTextAreaMovingDirectionEnum.Last:
                    if (Event.current.shift)
                        Last = to;
                    else
                        Last = First = to;
                    break;
            }
        }
        public void MoveDown(Rect position)
        {
            if (Length == 0)
                Direction = CustomTextAreaMovingDirectionEnum.Last;

            var content = new GUIContent(Text);
            int cursor = Direction == CustomTextAreaMovingDirectionEnum.First ? First : Last;
            var pos = Style.GetCursorPixelPosition(position, content, cursor);
            pos.y += Style.lineHeight;
            var to = Style.GetCursorStringIndex(position, content, pos);

            switch (Direction)
            {
                case CustomTextAreaMovingDirectionEnum.First:
                    if (Event.current.shift)
                        First = to;
                    else
                        Last = First = to;
                    break;
                case CustomTextAreaMovingDirectionEnum.Last:
                    if (Event.current.shift)
                        Last = to;
                    else
                        Last = First = to;
                    break;
            }
        }

        public void MouseDown(Rect position, Vector2 mouse)
        {
            var to = Style.GetCursorStringIndex(position, Content, mouse);
            if (Event.current.shift)
            {
                Direction = CustomTextAreaMovingDirectionEnum.Last;
                Last = to;
            }
            else
            {
                Last = First = to;
            }
        }
        public void MouseDoubleClick(Rect position, Vector2 mouse)
        {
            if (Event.current.control)
            {
                MouseTripleClick(position, mouse);
                return;
            }

            var strings = ExpressionUtility.ToStrings(Text);

            var index = Style.GetCursorStringIndex(position, Content, mouse);

            int i = 0;
            var to = 0;
            for (; i < strings.Count; i++)
            {
                if (to + strings[i].Length >= index)
                    break;
                else
                    to += strings[i].Length;
            }
            First = to;

            for (; i < strings.Count; i++)
            {
                to += strings[i].Length;
                if (to > index)
                    break;
            }
            Last = to;
        }
        public void MouseTripleClick(Rect position, Vector2 mouse)
        {
            var index = Style.GetCursorStringIndex(position, Content, mouse);

            int i = index;
            for (; i >= 0; i--)
            {
                if (Text[i] == '\n')
                    break;
            }
            First = i + 1;

            i = index; 
            for (; i < Text.Length; i++)
            {
                if (Text[i] == '\n')
                    break;
            }
            Last = i;
        }
        public void MouseDrag(Rect position, Vector2 mouse)
        {
            var to = Style.GetCursorStringIndex(position, Content, mouse);
            Direction = CustomTextAreaMovingDirectionEnum.Last;
            Last = to;
        }

        public void Duplicate()
        {
            if (Length > 0)
            {
                var to = Head + Length;
                var copy = Text.Substring(Head, Length);
                Text = Text.Insert(Head, copy);
                Last = First = to;
            }
            else
            {
                int i = Head < Text.Length ? Head : Text.Length - 1;
                
                if (Text[i] == '\n')
                    i--;

                for (; i > 0; i--)
                {
                    if (Text[i] == '\n')
                    {
                        i += 1;
                        break;
                    }
                }

                int j = Head < Text.Length ? Head : Text.Length - 1;
                for (; j < Text.Length; j++)
                {
                    if (Text[j] == '\n')
                    {
                        break;
                    }
                }

                var copy = Text.Substring(i, j - i);
                if (copy[copy.Length - 1] != '\n')
                    copy += '\n';
                Text = Text.Insert(i, copy);
                Last = First = Head + copy.Length;
            }
        }
        public void SlideDown()
        {
            var splits = Text.Split(new char[] { '\n' }, StringSplitOptions.None);
            int i = 0;
            for (int count = 0; i < splits.Length; i++)
            {
                if (count + splits[i].Length + 1 > Head)
                    break;
                else
                    count += splits[i].Length + 1;
            }
            if (i == splits.Length - 1)
                return;

            var copy = splits[i];
            splits[i] = splits[i + 1];
            splits[i + 1] = copy;

            StringBuilder build = new StringBuilder();
            for (int j = 0; j < splits.Length; j++)
            {
                build.AppendLine(splits[j]);
            }
            build.Remove(build.Length - 1, 1);
            Text = build.ToString();
        }
    }
}