using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SimpleVariables
{
    public class TestsWindow : EditorWindow
    {
        public static TestsWindow Current;
        public static SerializedObject SerializedObject;

        private static GUIContent Title
            => new GUIContent(ObjectNames.NicifyVariableName(nameof(TestsWindow)));

        public LiteralData Literal;
        public CustomTextArea TextArea;

        public static TestsWindow CreateWindow()
        {
            var window = CreateWindow<TestsWindow>();
            window.titleContent = Title;
            window.Literal = new LiteralData();
            window.TextArea = new CustomTextArea();
            window.TextArea.OnRepaint += window.Repaint;
            SerializedObject = new SerializedObject(window);
            return window;
        }
        [MenuItem("Window/Simple Variables/Tests Window")]
        public static void OpenWindow()
        {
            if (Current is null || Current.Equals(null))
            {
                Current = CreateWindow();
                Current.Show();
            }
            else
            {
                Current.Focus();
            }
        }

        public void OnGUI()
        {
            CustomEditorGUIUtility.CustomTextArea(TextArea);
            EditorGUILayout.TextField(string.Empty);
        }
    } 
}