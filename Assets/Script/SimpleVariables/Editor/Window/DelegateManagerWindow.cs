﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public class DelegateManagerWindow : EditorWindow
    {
        public static GUIContent TitleContent => new GUIContent(ObjectNames.NicifyVariableName(nameof(DelegateManager)));
        public static DelegateManagerWindow Current;
        [MenuItem("Window/Simple Variables/Delegate Manager")]
        public static void OpenWindow()
        {
            if (Current is null || Current.Equals(null))
            {
                Current = CreateWindow<DelegateManagerWindow>();
                Current.titleContent = TitleContent;
            }
            Current.Show();
        }
        public void OnGUI()
        {
            foreach (KeyValuePair<DelegateKey, DelegateData> pair in DelegateManager.Pairs)
            {
                DrawPair(pair);
            }
        }
        private void DrawPair(KeyValuePair<DelegateKey, DelegateData> pair)
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Literal");
            CustomEditorGUIUtility.TextArea(pair.Key.literal);
            if (pair.Key.environment is Object obj)
                EditorGUILayout.ObjectField(new GUIContent("Environment"), obj, typeof(Object), false);
            foreach (var owner in pair.Value.Owners)
                EditorGUILayout.ObjectField(new GUIContent("Owner"), owner.WithObject, typeof(Object), false);
            EditorGUILayout.EndVertical();
        }
    }
}