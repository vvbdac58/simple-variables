using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace SimpleVariables
{
    [CustomEditor(typeof(VariableComponent))]
    public class VariableComponentEditor : Editor
    {
        private VariableComponent Target
        { get; set; }
        private ReorderableList List
            => Target.CachedList;
        private SerializedObject SerializedObject
            => Target.CachedObject;

        public void OnEnable()
        {
            Target = target as VariableComponent;
        }
        public override void OnInspectorGUI()
        {
            SerializedObject.Update();
            List.DoLayoutList();
            SerializedObject.ApplyModifiedProperties();
        }
        public virtual void OnInspectorGUI(Rect rect)
        {
            SerializedObject.Update();
            List.DoList(rect);
            SerializedObject.ApplyModifiedProperties();
        }
    }
}