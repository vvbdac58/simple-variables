using System;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace SimpleVariables
{
    [CustomEditor(typeof(VariableAsset))]
    public class VariableAssetEditor : Editor
    {
        private VariableAsset Target
        { get; set; }
        private ReorderableList List
            => Target.CachedList;
        private SerializedObject SerializedObject
            => Target.CachedObject;

        public void OnEnable()
        {
            Target = target as VariableAsset;
            if (Target is object && !Target.Equals(null))
                Target.OnValidate();
        }
        public override void OnInspectorGUI()
        {
            if (!Target.IsMainAsset)
            {
                EditorGUILayout.LabelField("Sub Asset", CustomGUIStyles.BoldLabel);
                EditorGUILayout.Space();
             
                var mainProp = serializedObject.FindProperty(nameof(VariableAsset.MainAsset));
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.ObjectField(mainProp);
                EditorGUI.EndDisabledGroup();

                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.ObjectField(new GUIContent("Self"), Target, typeof(VariableAsset), false);
                    EditorGUI.EndDisabledGroup();
                    if (GUILayout.Button("Destroy", GUILayout.Width(60f)))
                    {
                        if (EditorUtility.DisplayDialog("Remove Subasset",
                            $"Are you sure you want to destroy {Target}?",
                            "Destroy",
                            "Cancel"))
                        {
                            string mainAssetPath = AssetDatabase.GetAssetPath(Target.MainAsset);
                            AssetDatabase.RemoveObjectFromAsset(Target);
                            DestroyImmediate(Target);
                            AssetDatabase.ImportAsset(mainAssetPath);
                            return;
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
            else
            {
                EditorGUILayout.LabelField("Main Asset", CustomGUIStyles.BoldLabel);
                EditorGUILayout.Space();
            }

            SerializedObject.Update();
            List.DoLayoutList();
            SerializedObject.ApplyModifiedProperties();
        }
        public virtual void OnInspectorGUI(Rect rect)
        {
            SerializedObject.Update();
            List.DoList(rect);
            SerializedObject.ApplyModifiedProperties();
        }
    }
}