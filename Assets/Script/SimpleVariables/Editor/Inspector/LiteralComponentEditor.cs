using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SimpleVariables
{
    [CustomEditor(typeof(LiteralComponent))]
    public class LiteralComponentEditor : Editor
    {
        LiteralComponent component;
        SerializedProperty literalProp;

        public void OnEnable()
        {
            component = target as LiteralComponent;
            literalProp = serializedObject.FindProperty(nameof(LiteralComponent.Literal));
        }
        public override void OnInspectorGUI()
        {
            if (component.Literal is object)
                component.Literal.PropertyLayout(literalProp, true);
        }
    }
}