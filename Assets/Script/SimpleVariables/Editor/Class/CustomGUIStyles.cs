using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SimpleVariables 
{
    public static class CustomGUIStyles
    {
        public static Color TextColor
            => EditorGUIUtility.isProSkin ? White : Black;

        public static readonly Color White;
        public static readonly Color Black;

        public static Color GreenGUIColor
        {
            get
            {
                var color = TextColor;
                color.b /= 1.1f;
                color.r /= 1.1f;
                color.g *= 1.3f;
                return color;
            }
        }
        public static Color YellowGUIColor
        {
            get
            {
                var color = TextColor;
                color.b /= 1.7f;
                color.r *= 1.7f;
                color.g *= 1.7f;
                return color;
            }
        }
        public static Color RedGUIColor
        {
            get
            {
                var color = TextColor;
                color.b /= 1.7f;
                color.r *= 1.7f;
                color.g /= 1.7f;
                return color;
            }
        }

        public static GUIStyle MiniLabel;
        public static GUIStyle MiniLowerRightHintLabel;
        public static GUIStyle BoldLabel;
        public static GUIStyle GreenBoldLabel;
        public static GUIStyle BoldCenteredLabel;

        public static GUIStyle TextField;
        public static GUIStyle GreenTextField;

        public static GUIStyle TextArea;

        static CustomGUIStyles()
        {
            White = new Color(0.8f, 0.8f, 0.8f, 1f);
            Black = new Color(0.2f, 0.2f, 0.2f, 1f);
            

            MiniLabel
                = new GUIStyle()
                {
                    fontSize = 9,
                };
            MiniLabel.normal.textColor = TextColor;


            MiniLowerRightHintLabel
                = new GUIStyle(MiniLabel)
                {
                    alignment = TextAnchor.LowerRight,
                    contentOffset = new Vector2(-3, 0),
                };
            var c = MiniLowerRightHintLabel.normal.textColor;
            c.a *= 0.7f;
            MiniLowerRightHintLabel.normal.textColor = c;


            BoldLabel
                = new GUIStyle()
                {
                    fontStyle = FontStyle.Bold,
                    alignment = TextAnchor.MiddleLeft,
                };
            BoldLabel.normal.textColor = TextColor;

            GreenBoldLabel
                = new GUIStyle(BoldLabel)
                {

                };
            GreenBoldLabel.normal.textColor = GreenGUIColor;


            BoldCenteredLabel
                = new GUIStyle(BoldLabel)
                {
                    alignment = TextAnchor.MiddleCenter,
                };


            TextField
                = new GUIStyle(EditorStyles.textField)
                { 
                
                };


            GreenTextField
                = new GUIStyle(TextField)
                {

                };
            GreenTextField.normal.textColor = GreenGUIColor;
            GreenTextField.hover.textColor = GreenGUIColor;
            GreenTextField.focused.textColor = GreenGUIColor;

            TextArea
                = new GUIStyle(EditorStyles.textArea)
                {

                };
        }
    }
}