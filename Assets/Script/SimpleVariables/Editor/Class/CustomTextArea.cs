﻿using System;
using UnityEditor;
using UnityEngine;

namespace SimpleVariables
{
    public enum CustomTextAreaMovingDirectionEnum
    {
        First,
        Last,
    }
    public class CustomTextArea
    {
        private CustomTextAreaData data = new CustomTextAreaData(string.Empty, CustomGUIStyles.TextArea);

        public string Name
        {   
            get => ObjectNames.NicifyVariableName($"{nameof(CustomTextArea)}:{GetHashCode()}");
        } 
        public string Text
        {
            get => data.Text;
            set
            {
                if (data.Text != value)
                {
                    data.Text = value;
                    Repaint();
                }
            }
        }
        public bool IsFocused
        {   
            get => Name == GUI.GetNameOfFocusedControl();
        }

        public event Action OnRepaint;

        public GUIContent Content
            => new GUIContent(Text);

        public void Repaint()
        {
            if (OnRepaint is object)
                OnRepaint.Invoke();
        }
        public void Focus()
        {
            EditorGUI.FocusTextInControl(Name);
        }
        private bool TryGetTypedCharacter(out char character)
        {
            bool lower = true;
            if (Event.current.capsLock)
                lower = Event.current.shift;
            else if (Event.current.shift)
                lower = Event.current.capsLock;

            var code = Event.current.character;
            character = '\0';
            if (code > 0)
            {
                character = Event.current.character;
                return true;
            }
            return false;
        }
        private void HandleKey(Rect position)
        {
            if (Event.current.type != EventType.KeyDown)
                return;

            if (!IsFocused)
                return;

            if (TryGetTypedCharacter(out char character))
            {
                data.Insert(character.ToString());
            }
            else
            {
                switch (Event.current.keyCode)
                {
                    case KeyCode.A:
                        if (Event.current.control)
                            data.SelectAll();
                        break;
                    case KeyCode.D:
                        if (Event.current.control)
                            data.Duplicate();
                        break;
                    case KeyCode.LeftArrow:
                        data.MoveLeft();
                        break;
                    case KeyCode.RightArrow:
                        data.MoveRight();
                        break;
                    case KeyCode.UpArrow:
                        data.MoveUp(position);
                        break;
                    case KeyCode.DownArrow:
                        if (Event.current.alt)
                            data.SlideDown();
                        else
                            data.MoveDown(position);
                        break;
                    case KeyCode.Delete:
                        data.Delete();
                        break;
                    case KeyCode.Backspace:
                        data.Backspace();
                        break;
                }
            }
            Repaint();
        }
        private void HandleClick(Rect position)
        {
            if (!Event.current.isMouse)
                return;

            var mouse = Event.current.mousePosition;
            if (!position.Contains(mouse))
                return;

            if (IsFocused)
            {
                if (Event.current.type == EventType.MouseDown)
                {
                    var clickCount = Event.current.clickCount;
                    if (clickCount == 1)
                        data.MouseDown(position, mouse);
                    else if (clickCount == 2)
                        data.MouseDoubleClick(position, mouse);
                    else if (clickCount == 3)
                        data.MouseTripleClick(position, mouse);
                }
                else if (Event.current.type == EventType.MouseDrag)
                {
                    data.MouseDrag(position, mouse);
                }
            }
            else
            {
                Focus();
                data.SelectAll();
            }

            if (OnRepaint is object)
                OnRepaint.Invoke();
        }
        private void DrawContent(Rect position, int id)
        {
            if (Event.current.type == EventType.Repaint)
            {
                if (IsFocused)
                {
                    data.Style.DrawWithTextSelection(position, Content, id, data.First, data.Last);
                }
                else
                {
                    var mouse = Event.current.mousePosition;
                    data.Style.Draw(position, Content, id, false, position.Contains(mouse));
                }
            }
        }
        public void Layout()
        {
            float height = CustomEditorGUIUtility.TextAreaGetSize(Text).y;
            Rect rect = EditorGUILayout.GetControlRect(false, height);

            GUI.SetNextControlName(Name);
            var id = EditorGUIUtility.GetControlID(FocusType.Keyboard, rect);
            EditorGUIUtility.AddCursorRect(rect, MouseCursor.Text, id);

            HandleClick(rect);
            HandleKey(rect);
            DrawContent(rect, id);
        }
    }
}