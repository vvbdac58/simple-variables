using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace SimpleVariables
{
    public static class CustomEditorGUIUtility
    {
        public static void TextArea(SerializedProperty property)
        {
            float height = TextAreaGetSize(property).y;
            Rect rect = EditorGUILayout.GetControlRect(false, height);
            EditorGUI.BeginProperty(rect, new GUIContent(property.displayName), property);
            property.stringValue = EditorGUI.TextArea(rect, property.stringValue);
            EditorGUI.EndProperty();
        }
        public static string TextArea(string text)
        {
            float height = TextAreaGetSize(text).y;
            Rect rect = EditorGUILayout.GetControlRect(false, height);
            return EditorGUI.TextArea(rect, text);
        }
        public static void TextArea(Rect position, SerializedProperty property)
        {
            var pos = position.position;
            var size = position.size;
            {
                Rect rect = new Rect(pos, size);
                EditorGUI.BeginProperty(rect, new GUIContent(property.displayName), property);
                property.stringValue = EditorGUI.TextArea(rect, property.stringValue);
                EditorGUI.EndProperty();
            }
        }
        public static Vector2 TextAreaGetSize(string text)
        {
            float width = EditorGUIUtility.currentViewWidth;
            float height = CustomGUIStyles.TextArea.CalcHeight(new GUIContent(text), width);
            return new Vector2(width, height);
        }
        public static Vector2 TextAreaGetSize(SerializedProperty property)
        {
            float width = EditorGUIUtility.currentViewWidth;
            float height = CustomGUIStyles.TextArea.CalcHeight(new GUIContent(property.stringValue), width);
            return new Vector2(width, height);
        }
        public static Vector2 TextAreaGetSize(SerializedProperty property, float width)
        {
            float height = CustomGUIStyles.TextArea.CalcHeight(new GUIContent(property.stringValue), width);
            return new Vector2(width, height);
        }

        public static void CustomTextArea(CustomTextArea area)
        {
            if (area is null)
                return;
            area.Layout();
        }
    }
}