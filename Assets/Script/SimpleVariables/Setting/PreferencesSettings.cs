using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace SimpleVariables
{
    public class PreferencesSettings : ScriptableObject
    {
        public const string FOLDER_PATH = "Assets/Preferences";
        public const string ASSET_PATH = "Assets/Preferences/SimpleVariables.asset";

        public List<string> UsingAssemblies = new List<string>();

        private static SerializedObject cachedObject;
        private static ReorderableList assembliesList;

        public static PreferencesSettings Instance
        {
            get
            {
                PreferencesSettings instance = AssetDatabase.LoadAssetAtPath<PreferencesSettings>(ASSET_PATH);
                if (instance is null || instance.Equals(null))
                {
                    instance = CreateInstance<PreferencesSettings>();
                    Directory.CreateDirectory(FOLDER_PATH);
                    AssetDatabase.CreateAsset(instance, ASSET_PATH);
                }
                return instance;
            }
        }
        public static SerializedObject CachedObject
        {
            get
            {
                if (cachedObject is null)
                    cachedObject = new SerializedObject(Instance);
                return cachedObject;
            }
        }
        public static ReorderableList AssembliesList
        {
            get
            {
                if (assembliesList is null)
                {
                    var assembliesProp = CachedObject.FindProperty(nameof(UsingAssemblies));
                    assembliesList = new ReorderableList(CachedObject, assembliesProp, true, false, true, true)
                    {
                        drawElementCallback = drawElement,
                    };

                    void drawElement(Rect rect, int index, bool isActive, bool isFocused)
                    {
                        var item = assembliesProp.GetArrayElementAtIndex(index);
                        var pos = rect.position;
                        var size = rect.size;
                        { 
                            var labelSize = new Vector2(EditorGUIUtility.labelWidth, size.y);
                            var labelRect = new Rect(pos, labelSize);
                            EditorGUI.LabelField(labelRect, new GUIContent(item.displayName));
                        }
                        {
                            var dropPos = new Vector2(pos.x + EditorGUIUtility.labelWidth, pos.y + 3f);
                            var dropSize = new Vector2(size.x - EditorGUIUtility.labelWidth, size.y);
                            var dropRect = new Rect(dropPos, dropSize);
                            if (EditorGUI.DropdownButton(dropRect, new GUIContent(item.stringValue), FocusType.Passive))
                            {
                                GenericMenu menu = new GenericMenu();
                                var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                                foreach (var assem in assemblies)
                                {
                                    var name = assem.GetName().Name;
                                    var content = new GUIContent(name.Replace(".", "/"));
                                    menu.AddItem(content,
                                        name == item.stringValue,
                                        () => 
                                        { 
                                            item.stringValue = name;
                                            CachedObject.ApplyModifiedProperties();
                                        });
                                }
                                menu.ShowAsContext();
                            }
                        }
                    }
                }
                return assembliesList;
            }
        }

        [PreferenceItem("Simple Variables")]
        public static void OnPreferences()
        {
            var obj = CachedObject;
            obj.Update();
            {
                AssembliesList.DoLayoutList();
            }
            obj.ApplyModifiedProperties();
        }
    }
/*
    static class ProjectSettingsProvider
    {
        [SettingsProvider]
        public static SettingsProvider CreateMyCustomSettingsProvider()
        {
            // First parameter is the path in the Settings window.
            // Second parameter is the scope of this setting: it only appears in the Project Settings window.
            var provider = new SettingsProvider("Preferences/Simple Variables", SettingsScope.Project)
            {
                // By default the last token of the path is used as display name if no label is provided.
                label = "Simple Variables",
                // Create the SettingsProvider and initialize its drawing (IMGUI) function in place:
                guiHandler = (searchContext) =>
                {
                    EditorGUILayout.LabelField("Test");
                },
            };

            

            return provider;
        }
    }
*/
}