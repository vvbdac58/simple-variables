﻿using System;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
#endif

namespace SimpleVariables
{
    public class FsmStringVariable : BaseFsmVariable
    {
        public FsmStringVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmString FsmString
        {
            get => OwnerFsm.FsmVariables.FindFsmString(Name);
        }
        public string String
        {
            get => FsmString.Value;
            set => FsmString.Value = value;
        }
        public override object Value 
        { 
            get => FsmString.RawValue; 
            set => FsmString.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(String);
        public override Type ValueType 
            => typeof(string);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.TextField(position, label, String);
        }
#endif
    }
}