﻿using System;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
#endif

namespace SimpleVariables
{
    public class FsmEnumVariable : BaseFsmVariable
    {
        public FsmEnumVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmEnum FsmEnum
        {
            get => OwnerFsm.FsmVariables.FindFsmEnum(Name);
        }
        public Enum Enum
        {
            get => FsmEnum.Value;
            set => FsmEnum.Value = value;
        }
        public override object Value 
        { 
            get => FsmEnum.RawValue; 
            set => FsmEnum.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Enum);
        public override Type ValueType 
            => typeof(Enum);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.EnumPopup(position, label, Enum);
        }
#endif
    }
}