﻿using System;
using UnityEngine;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor; 
#endif

namespace SimpleVariables
{
    public class FsmBoolVariable : BaseFsmVariable
    {
        public FsmBoolVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmBool FsmBool
        {
            get => OwnerFsm.FsmVariables.FindFsmBool(Name);
        }
        public bool Bool
        {
            get => FsmBool.Value;
            set => FsmBool.Value = value;
        }
        public override object Value 
        { 
            get => FsmBool.RawValue; 
            set => FsmBool.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Bool);
        public override Type ValueType 
            => typeof(bool);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.Toggle(position, label, Bool);
        }
#endif
    }
}