﻿using System;
using UnityEngine;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public class FsmGameObjectVariable : BaseFsmVariable
    {
        public FsmGameObjectVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmGameObject FsmGameObject
        {
            get => OwnerFsm.FsmVariables.FindFsmGameObject(Name);
        }
        public GameObject GameObject
        {
            get => FsmGameObject.Value;
            set => FsmGameObject.Value = value;
        }
        public override object Value 
        { 
            get => FsmGameObject.RawValue; 
            set => FsmGameObject.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(GameObject);
        public override Type ValueType 
            => typeof(GameObject);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.ObjectField(position, label, GameObject, typeof(GameObject), true);
        }
#endif
    }
}