﻿using System;
using UnityEngine;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public class FsmVector3Variable : BaseFsmVariable
    {
        public FsmVector3Variable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmVector3 FsmVector3
        {
            get => OwnerFsm.FsmVariables.FindFsmVector3(Name);
        }
        public Vector3 Vector3
        {
            get => FsmVector3.Value;
            set => FsmVector3.Value = value;
        }
        public override object Value 
        { 
            get => FsmVector3.RawValue; 
            set => FsmVector3.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Vector3);
        public override Type ValueType 
            => typeof(Vector3);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.Vector3Field(position, label, Vector3);
        }
#endif
    }
}