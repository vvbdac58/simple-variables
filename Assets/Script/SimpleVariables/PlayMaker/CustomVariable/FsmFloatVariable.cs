using HutongGames.PlayMaker;
using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
#endif

namespace SimpleVariables
{
    public class FsmFloatVariable : BaseFsmVariable
    {
        public FsmFloatVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmFloat FsmFloat
        {
            get => OwnerFsm.FsmVariables.FindFsmFloat(Name);
        }
        public float Float
        {
            get => FsmFloat.Value;
            set => FsmFloat.Value = value;
        }
        public override object Value 
        { 
            get => FsmFloat.RawValue; 
            set => FsmFloat.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Float);
        public override Type ValueType 
            => typeof(float);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.FloatField(position, label, Float);
        }
#endif
    }
}