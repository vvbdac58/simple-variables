﻿using System;
using UnityEngine;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public class FsmTextureVariable : BaseFsmVariable
    {
        public FsmTextureVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmTexture FsmTexture
        {
            get => OwnerFsm.FsmVariables.FindFsmTexture(Name);
        }
        public Texture Texture
        {
            get => FsmTexture.Value;
            set => FsmTexture.Value = value;
        }
        public override object Value 
        { 
            get => FsmTexture.RawValue; 
            set => FsmTexture.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Texture);
        public override Type ValueType 
            => typeof(Texture);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.ObjectField(position, label, Texture, ValueType, false);
        }
#endif
    }
}