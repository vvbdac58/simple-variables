﻿using System;
using UnityEngine;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public class FsmRectVariable : BaseFsmVariable
    {
        public FsmRectVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmRect FsmRect
        {
            get => OwnerFsm.FsmVariables.FindFsmRect(Name);
        }
        public Rect Rect
        {
            get => FsmRect.Value;
            set => FsmRect.Value = value;
        }
        public override object Value 
        { 
            get => FsmRect.RawValue; 
            set => FsmRect.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Rect);
        public override Type ValueType 
            => typeof(Rect);
#if UNITY_EDITOR
        public override float GetPropertyHeight(SerializedProperty prop, float width)
        {
            if (IsExpanded)
                return
                    base.GetPropertyHeight(prop, width) +
                    (spacing +
                    lineHeight) * 2f;
            else
                return
                    base.GetPropertyHeight(prop, width);
        }
        public override void PropertyField(Rect position, SerializedProperty prop, bool editable, out Vector2 pos, out Vector2 size)
        {
            base.PropertyField(position, prop, editable, out pos, out size);

            if (IsExpanded)
            {
                EditorGUI.indentLevel++;
                pos.y += spacing + size.y;
                size = new Vector2(position.width, lineHeight * 2f + spacing);
                {
                    var rect = new Rect(pos, size);
                    var label = new GUIContent(ObjectNames.NicifyVariableName(ValueFieldName));
                    EditorGUI.BeginDisabledGroup(!editable);
                    Value = ValueField(rect, label, Value);
                    EditorGUI.EndDisabledGroup();
                }
                EditorGUI.indentLevel--;
            }
        }
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.RectField(position, label, Rect);
        }
#endif
    }
}