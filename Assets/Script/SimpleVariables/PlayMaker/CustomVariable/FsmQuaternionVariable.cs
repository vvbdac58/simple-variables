﻿using System;
using UnityEngine;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public class FsmQuaternionVariable : BaseFsmVariable
    {
        public FsmQuaternionVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmQuaternion FsmQuaternion
        {
            get => OwnerFsm.FsmVariables.FindFsmQuaternion(Name);
        }
        public Quaternion Quaternion
        {
            get => FsmQuaternion.Value;
            set => FsmQuaternion.Value = value;
        }
        public override object Value 
        { 
            get => FsmQuaternion.RawValue; 
            set => FsmQuaternion.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Quaternion);
        public override Type ValueType 
            => typeof(Quaternion);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            var euler = EditorGUI.Vector3Field(position, label, Quaternion.eulerAngles);
            return Quaternion.Euler(euler);
        }
#endif
    }
}