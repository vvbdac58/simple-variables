﻿using System;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
#endif
using Object = UnityEngine.Object;

namespace SimpleVariables
{
    public class FsmObjectVariable : BaseFsmVariable
    {
        public FsmObjectVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmObject FsmObject
        {
            get => OwnerFsm.FsmVariables.FindFsmObject(Name);
        }
        public Object Object
        {
            get => FsmObject.Value;
            set => FsmObject.Value = value;
        }
        public override object Value 
        { 
            get => FsmObject.RawValue; 
            set => FsmObject.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Object);
        public override Type ValueType 
            => typeof(Object);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.ObjectField(position, label, Object, FsmObject.ObjectType, true);
        }
#endif
    }
}