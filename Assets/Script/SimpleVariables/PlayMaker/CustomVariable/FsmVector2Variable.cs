﻿using System;
using UnityEngine;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public class FsmVector2Variable : BaseFsmVariable
    {
        public FsmVector2Variable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmVector2 FsmVector2
        {
            get => OwnerFsm.FsmVariables.FindFsmVector2(Name);
        }
        public Vector2 Vector2
        {
            get => FsmVector2.Value;
            set => FsmVector2.Value = value;
        }
        public override object Value 
        { 
            get => FsmVector2.RawValue; 
            set => FsmVector2.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Vector2);
        public override Type ValueType 
            => typeof(Vector2);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.Vector2Field(position, label, Vector2);
        }
#endif
    }
}