﻿using HutongGames.PlayMaker;
using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public class FsmMaterialVariable : BaseFsmVariable
    {
        public FsmMaterialVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmMaterial FsmMaterial
        {
            get => OwnerFsm.FsmVariables.FindFsmMaterial(Name);
        }
        public Material Material
        {
            get => FsmMaterial.Value;
            set => FsmMaterial.Value = value;
        }
        public override object Value 
        { 
            get => FsmMaterial.RawValue; 
            set => FsmMaterial.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Material);
        public override Type ValueType 
            => typeof(Material);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.ObjectField(position, label, Material, typeof(Material), false);
        }
#endif
    }
}