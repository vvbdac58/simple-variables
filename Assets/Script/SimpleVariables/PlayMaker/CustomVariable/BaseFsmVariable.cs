using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker;
using System;
using UnityEditor;
using Object = UnityEngine.Object;

namespace SimpleVariables
{
    public class BaseFsmVariable : BaseVariable
    {
        public PlayMakerFSM OwnerFsm;

        public BaseFsmVariable(PlayMakerFSM fsm, NamedVariable named)
        {
            OwnerFsm = fsm;
            Name = named.Name;
            Hint = named.Tooltip;
        }

        public NamedVariable Named
        {
            get => OwnerFsm.FsmVariables.FindVariable(Name);
        }
        public override object Value 
        { 
            get
            {
                return Named.RawValue;
            }
            set
            {
                Named.RawValue = value;
            }
        }
        public override string ValueFieldName
            => "Value";
        public override Type ValueType
        { 
            get
            {
                if (Value is null)
                {
                    var named = Named;
                    var varType = named.VariableType;
                    if (varType == VariableType.String)
                    {
                        return typeof(string);
                    }
                    else if (varType == VariableType.GameObject)
                    {
                        return typeof(GameObject);
                    }
                    else if (varType == VariableType.Material || varType == VariableType.Object || varType == VariableType.Texture)
                    {
                        return Named.ObjectType;
                    }
                    else
                    {
                        throw new TypeLoadException($"{Named.Name}'s type is {varType}, being unkown!");
                    }
                }
                else
                { 
                    return Value.GetType(); 
                }
            }
        }

#if UNITY_EDITOR
        public override float GetPropertyHeight(SerializedProperty prop, float width)
        {
            if (IsExpanded)
                return
                    base.GetPropertyHeight(prop, width) + 
                    spacing +
                    lineHeight;
            else
                return
                    base.GetPropertyHeight(prop, width);
        }
        public virtual object ValueField(Rect position, GUIContent label, object value)
        {
            return value;
        }
        public override void PropertyField(Rect position, SerializedProperty prop, bool editable, out Vector2 pos, out Vector2 size)
        {
            base.PropertyField(position, prop, editable, out pos, out size);

            if (IsExpanded)
            {
                EditorGUI.indentLevel++;
                pos.y += spacing + size.y;
                size = new Vector2(position.width, lineHeight);
                {
                    var rect = new Rect(pos, size);
                    var label = new GUIContent(ObjectNames.NicifyVariableName(ValueFieldName));
                    EditorGUI.BeginDisabledGroup(!editable);
                    Value = ValueField(rect, label, Value);
                    EditorGUI.EndDisabledGroup();
                }
                EditorGUI.indentLevel--;
            }
        }
#endif
    }
}