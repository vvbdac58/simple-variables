﻿using HutongGames.PlayMaker;
using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public class FsmColorVariable : BaseFsmVariable
    {
        public FsmColorVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmColor FsmColor
        {
            get => OwnerFsm.FsmVariables.FindFsmColor(Name);
        }
        public Color Color
        {
            get => FsmColor.Value;
            set => FsmColor.Value = value;
        }
        public override object Value 
        { 
            get => FsmColor.RawValue; 
            set => FsmColor.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Color);
        public override Type ValueType 
            => typeof(Color);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.ColorField(position, label, Color);
        }
#endif
    }
}