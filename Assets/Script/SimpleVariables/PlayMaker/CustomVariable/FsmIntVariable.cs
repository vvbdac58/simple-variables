﻿using System;
using HutongGames.PlayMaker;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
#endif

namespace SimpleVariables
{
    public class FsmIntVariable : BaseFsmVariable
    {
        public FsmIntVariable(PlayMakerFSM fsm, NamedVariable named) : base(fsm, named)
        {

        }
        public FsmInt FsmInt
        {
            get => OwnerFsm.FsmVariables.FindFsmInt(Name);
        }
        public int Int
        {
            get => FsmInt.Value;
            set => FsmInt.Value = value;
        }
        public override object Value 
        { 
            get => FsmInt.RawValue; 
            set => FsmInt.RawValue = value;
        }
        public override string ValueFieldName
            => nameof(Int);
        public override Type ValueType 
            => typeof(int);
#if UNITY_EDITOR
        public override object ValueField(Rect position, GUIContent label, object value)
        {
            return EditorGUI.IntField(position, label, Int);
        }
#endif
    }
}