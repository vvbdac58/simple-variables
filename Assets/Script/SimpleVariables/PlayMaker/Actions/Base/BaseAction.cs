#if PLAYMAKER
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker;

namespace SimpleVariables
{
    /// <summary>
    /// A base class for action inheritance.
    /// </summary>
    public abstract class BaseAction : FsmStateAction
    {
        /// <summary>
        /// Tells this action will do in OnUpdate()
        /// </summary>
        [HutongGames.PlayMaker.Tooltip("Will do in OnUpdate()")]
        [DisplayOrder(100)]
        public FsmBool EveryFrame = new FsmBool();

        /// <summary>
        /// Called in OnEnter and OnUpdate.
        /// </summary>
        public abstract void Execute();
        /// <summary>
        /// Called when action got entered, finished when it's not every frame.
        /// </summary>
        public override void OnEnter()
        {
            Execute();
            if (!EveryFrame.Value)
                Finish();
        }
        /// <summary>
        /// Called when action got updated, finished when it's not every frame.
        /// </summary>
        public override void OnUpdate()
        {
            Execute();
            if (!EveryFrame.Value)
                Finish();
        }
    }
}
#endif