#if PLAYMAKER
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker;

namespace SimpleVariables
{
    /// <summary>
    /// A class used to implement action with a certain component to invoke action.
    /// </summary>
    /// <typeparam name="T"> type of the component </typeparam>
    public abstract class ComponentBaseAction<T> : BaseAction where T : Component
    {
        /// <summary>
        /// Selects the owner of Component.
        /// </summary>
        [HutongGames.PlayMaker.Tooltip("Owner of Component")]
        [DisplayOrder(0)]
        public FsmOwnerDefault OwnerDefault = new FsmOwnerDefault();

        /// <summary>
        /// Stored component reference in GetComponent().
        /// </summary>
        protected T Component;
        /// <summary>
        /// Gets the component.
        /// </summary>
        public virtual void GetComponent()
        {
            var gameObject = Fsm.GetOwnerDefaultTarget(OwnerDefault);
            gameObject.TryGetComponent(out Component);
        }
        public override void OnEnter()
        {
            GetComponent();
            base.OnEnter();
        }
    }
}
#endif