using System;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SimpleVariables
{
    [ActionCategory("SimpleVariables")]
    public class LiteralExecute : BaseAction, IDisposable
    {
        [ObjectType(typeof(FsmEnvironmentReferencingEnum))]
        public FsmEnum Referencing = new FsmEnum();
        [ObjectType(typeof(VariableAsset))]
        public FsmObject ReferencedAsset = new FsmObject();
        [ObjectType(typeof(VariableComponent))]
        public FsmObject ReferencedComponent = new FsmObject();
        [UIHint(UIHint.TextArea)]
        public FsmString Literal = new FsmString(string.Empty);

        private LiteralData literal;
        private IVariableEnvironment environment
        {
            get
            {
                switch ((FsmEnvironmentReferencingEnum)Referencing.Value)
                {
                    case FsmEnvironmentReferencingEnum.Fsm:
                        if (Fsm.FsmComponent.TryGetEnvironment(out var env))
                            return env;
                        return null;
                    case FsmEnvironmentReferencingEnum.Asset:
                        return (VariableAsset)ReferencedAsset.Value;
                    case FsmEnvironmentReferencingEnum.Component:
                        return (VariableComponent)ReferencedComponent.Value;
                    default:
                        throw new InvalidOperationException();
                }
            }
        }

        public virtual void OnInit()
        {
            if (literal is object)
            {
                foreach (var used in literal.Data.Variables)
                    used.AddUsingObject(Fsm.FsmComponent);
                return;
            }
            {
                if (Fsm.GameObject.TryGetComponent(out FsmLiteralExecuteComponent comp))
                {
                    comp.OnDestroyDelayedEvent += Dispose;
                }
                else
                {
                    comp = Fsm.GameObject.AddComponent<FsmLiteralExecuteComponent>();
                    comp.OnDestroyDelayedEvent += Dispose;
                }

                literal = new LiteralData(Fsm.FsmComponent, Literal.Value, environment);
                literal.OnInit();
            }
        }

        public override void OnEnter()
        {
            OnInit();

            base.OnEnter();
        }

        public override void Execute()
        {
            literal.Invoke();
        }

        public void Dispose()
        {
            literal.Dispose();
        }

        public override void OnExit()
        {
            if (literal is null)
                return;
            foreach (var used in literal.Data.Variables)
                used.RemoveUsingObject(Fsm.FsmComponent);
        }
    }
}