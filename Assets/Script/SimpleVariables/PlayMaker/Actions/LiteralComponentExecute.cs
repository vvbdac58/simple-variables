using HutongGames.PlayMaker;

namespace SimpleVariables
{
    [ActionCategory("SimpleVariables")]
    [ActionTarget(typeof(LiteralComponent), "OwnerDefault")]
    public class LiteralComponentExecute : ComponentBaseAction<LiteralComponent>
    {
        public override void Execute()
        {
            Component.Literal.Invoke();
        }
    }
}