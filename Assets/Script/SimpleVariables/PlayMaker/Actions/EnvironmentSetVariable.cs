using System;
using System.Collections.Generic;
using HutongGames.PlayMaker;
using UnityEngine;

namespace SimpleVariables
{
    [ActionCategory("SimpleVariables")]
    public class EnvironmentSetVariable : BaseAction
    {
        [ObjectType(typeof(EnvironmentReferencingEnum))]
        public FsmEnum Referencing = new FsmEnum();
        [ObjectType(typeof(VariableAsset))]
        public FsmObject ReferencedAsset = new FsmObject();
        [ObjectType(typeof(VariableComponent))]
        public FsmObject ReferencedComponent = new FsmObject();

        public FsmVarGetSet[] Sets = new FsmVarGetSet[0];

        private Dictionary<string, BaseVariable> baseVariables;

        private IVariableEnvironment environment
        {
            get
            {
                switch ((EnvironmentReferencingEnum)Referencing.Value)
                {
                    case EnvironmentReferencingEnum.Asset:
                        return (VariableAsset)ReferencedAsset.Value;
                    case EnvironmentReferencingEnum.Component:
                        return (VariableComponent)ReferencedComponent.Value;
                    default:
                        throw new InvalidOperationException();
                }
            }
        }

        void SetVariables()
        {
            for (int i = 0; i < Sets.Length; i++)
            {
                var name = Sets[i].VariableName.Value;
                if (baseVariables.TryGetValue(name, out var iBase))
                {
                    iBase.Value = Sets[i].FsmVariable.NamedVar.RawValue;
                    iBase.UsedFrame = Time.frameCount;
                }
            }
        }
        void GetVariables()
        {
            if (baseVariables is object)
                return;

            baseVariables = new Dictionary<string, BaseVariable>();
            var env = environment;
            for (int i = 0; i < Sets.Length; i++)
            {
                var name = Sets[i].VariableName.Value;
                if (env.TryGetVariable(name, out var iBase))
                {
                    baseVariables.Add(name, iBase);
                    iBase.AddUsingObject(Fsm.FsmComponent);
                }
            }
        }
        public override void Execute()
        {
            GetVariables();
            SetVariables();
        }

        public override void OnExit()
        {
            if (baseVariables is null)
                return;

            foreach (var iBase in baseVariables)
                iBase.Value.RemoveUsingObject(Fsm.FsmComponent);
        }
    }
}