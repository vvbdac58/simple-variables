using System;
using System.Collections.Generic;
using HutongGames.PlayMaker;
using UnityEngine;

namespace SimpleVariables
{
    [ActionCategory("SimpleVariables")]
    public class EnvironmentGetVariable : BaseAction
    {
        [ObjectType(typeof(EnvironmentReferencingEnum))]
        public FsmEnum Referencing = new FsmEnum();
        [ObjectType(typeof(VariableAsset))]
        public FsmObject ReferencedAsset = new FsmObject();
        [ObjectType(typeof(VariableComponent))]
        public FsmObject ReferencedComponent = new FsmObject();

        public FsmVarGetSet[] Gets = new FsmVarGetSet[0];

        private Dictionary<string, BaseVariable> baseVariables;

        private IVariableEnvironment environment
        {
            get
            {
                switch ((EnvironmentReferencingEnum)Referencing.Value)
                {
                    case EnvironmentReferencingEnum.Asset:
                        return (VariableAsset)ReferencedAsset.Value;
                    case EnvironmentReferencingEnum.Component:
                        return (VariableComponent)ReferencedComponent.Value;
                    default:
                        throw new InvalidOperationException();
                }
            }
        }

        void SetVariables()
        {
            for (int i = 0; i < Gets.Length; i++)
            {
                var name = Gets[i].VariableName.Value;
                if (baseVariables.TryGetValue(name, out var iBase))
                {
                    Gets[i].FsmVariable.SetValue(iBase.Value);
                    iBase.UsedFrame = Time.frameCount;
                }
            }
        }
        void GetVariables()
        {
            if (baseVariables is object)
                return;

            baseVariables = new Dictionary<string, BaseVariable>();
            var env = environment;
            for (int i = 0; i < Gets.Length; i++)
            {
                var name = Gets[i].VariableName.Value;
                if (env.TryGetVariable(name, out var iBase))
                {
                    baseVariables.Add(name, iBase);
                    iBase.AddUsingObject(Fsm.FsmComponent);
                }
            }
        }
        public override void Execute()
        {
            GetVariables();
            SetVariables();
        }

        public override void OnExit()
        {
            if (baseVariables is null)
                return;

            foreach (var iBase in baseVariables)
                iBase.Value.RemoveUsingObject(Fsm.FsmComponent);
        }
    }
}