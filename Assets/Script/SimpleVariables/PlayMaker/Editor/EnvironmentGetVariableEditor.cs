using UnityEngine;
using UnityEditor;
using HutongGames.PlayMakerEditor;
using UnityEditorInternal;
using HutongGames.PlayMaker;

namespace SimpleVariables
{
    [CustomActionEditor(typeof(EnvironmentGetVariable))]
    public class EnvironmentGetVariableEditor : CustomActionEditor
    {
        EnvironmentGetVariable action;

        public override void OnEnable()
        {
            action = target as EnvironmentGetVariable;
        }
        public override bool OnGUI()
        {
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginDisabledGroup(Application.isPlaying);
            {
                EditField(nameof(EnvironmentGetVariable.Referencing));
                EditorGUI.BeginDisabledGroup(((EnvironmentReferencingEnum)action.Referencing.Value) != EnvironmentReferencingEnum.Asset);
                EditField(nameof(EnvironmentGetVariable.ReferencedAsset));
                EditorGUI.EndDisabledGroup();
                EditorGUI.BeginDisabledGroup(((EnvironmentReferencingEnum)action.Referencing.Value) != EnvironmentReferencingEnum.Component);
                EditField(nameof(EnvironmentGetVariable.ReferencedComponent));
                EditorGUI.EndDisabledGroup();
            }
            {
                FsmEditorGUILayout.Spacer();
                EditField(nameof(EnvironmentGetVariable.Gets));
            }
            {
                EditField(nameof(EnvironmentGetVariable.EveryFrame));
            }

            EditorGUI.EndDisabledGroup();
            return EditorGUI.EndChangeCheck();
        }
    }
}