using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using HutongGames.PlayMakerEditor;
using UnityEngine.UIElements;

namespace SimpleVariables
{
    [CustomActionEditor(typeof(LiteralExecute))]
    public class LiteralExecuteEditor : CustomActionEditor
    {
        LiteralExecute action;
        public override void OnEnable()
        {
            action = target as LiteralExecute;
        }
        public override bool OnGUI()
        {
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginDisabledGroup(Application.isPlaying);
            if (action.Literal is object)
            {
                EditField(nameof(LiteralExecute.Referencing));
                var referencing = (FsmEnvironmentReferencingEnum)action.Referencing.Value;
                if (referencing != FsmEnvironmentReferencingEnum.Fsm)
                {
                    EditorGUI.BeginDisabledGroup(referencing != FsmEnvironmentReferencingEnum.Asset);
                    EditField(nameof(LiteralExecute.ReferencedAsset));
                    EditorGUI.EndDisabledGroup();
                    EditorGUI.BeginDisabledGroup(referencing != FsmEnvironmentReferencingEnum.Component);
                    EditField(nameof(LiteralExecute.ReferencedComponent));
                    EditorGUI.EndDisabledGroup();
                }

                {
                    EditField(nameof(LiteralExecute.Literal));
                }

                {
                    EditField(nameof(LiteralExecute.EveryFrame));
                }
            }
            EditorGUI.EndDisabledGroup();
            if (GUILayout.Button(DelegateManagerWindow.TitleContent))
                DelegateManagerWindow.OpenWindow();
            return EditorGUI.EndChangeCheck();
        }
    }
}