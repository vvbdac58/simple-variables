﻿using UnityEngine;
using UnityEditor;
using HutongGames.PlayMakerEditor;

namespace SimpleVariables
{
    [CustomActionEditor(typeof(EnvironmentSetVariable))]
    public class EnvironmentSetVariableEditor : CustomActionEditor
    {
        EnvironmentSetVariable action;

        public override void OnEnable()
        {
            action = target as EnvironmentSetVariable;
        }
        public override bool OnGUI()
        {
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginDisabledGroup(Application.isPlaying);
            {
                EditField(nameof(EnvironmentSetVariable.Referencing));
                EditorGUI.BeginDisabledGroup(((EnvironmentReferencingEnum)action.Referencing.Value) != EnvironmentReferencingEnum.Asset);
                EditField(nameof(EnvironmentSetVariable.ReferencedAsset));
                EditorGUI.EndDisabledGroup();
                EditorGUI.BeginDisabledGroup(((EnvironmentReferencingEnum)action.Referencing.Value) != EnvironmentReferencingEnum.Component);
                EditField(nameof(EnvironmentSetVariable.ReferencedComponent));
                EditorGUI.EndDisabledGroup();
            }
            {
                FsmEditorGUILayout.Spacer();
                EditField(nameof(EnvironmentSetVariable.Sets));
            }
            {
                EditField(nameof(EnvironmentSetVariable.EveryFrame));
            }

            EditorGUI.EndDisabledGroup();
            return EditorGUI.EndChangeCheck();
        }
    }
}