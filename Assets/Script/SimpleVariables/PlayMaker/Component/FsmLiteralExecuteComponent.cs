using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace SimpleVariables 
{
    public class FsmLiteralExecuteComponent : MonoBehaviour
    {
        public Dictionary<PlayMakerFSM, VariableAsset> Pairs = new Dictionary<PlayMakerFSM, VariableAsset>();
        public event Action OnDestroyDelayedEvent;
        public void OnDestroy()
        {
            foreach (var pair in Pairs)
                Destroy(pair.Value);

            DelayedEvent();
        }
        private async Task DelayedEvent()
        {
            await Task.Yield();
            if (OnDestroyDelayedEvent is object)
                OnDestroyDelayedEvent.Invoke();
            OnDestroyDelayedEvent = null;
        }
    } 
}