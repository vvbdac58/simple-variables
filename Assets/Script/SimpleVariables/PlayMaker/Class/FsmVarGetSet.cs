﻿using UnityEngine;
using HutongGames.PlayMaker;

#if UNITY_EDITOR
using UnityEditor;
using HutongGames.PlayMakerEditor; 
#endif

namespace SimpleVariables
{
    public class FsmVarGetSet
    {
        [SerializeField]
        public FsmString VariableName = new FsmString();
        [SerializeField]
        [UIHint(UIHint.Variable)]
        public FsmVar FsmVariable = new FsmVar() { useVariable = true };

        public void SetFsmValue(IVariableEnvironment env, Fsm fsm)
        {
            if (env.TryGetVariable(VariableName.Value, out var iBase))
            {
                FsmVariable.SetValue(iBase.Value);
            }
        }
        public void SetEnvironmentValue(IVariableEnvironment env, Fsm fsm)
        {
            if (env.TryGetVariable(VariableName.Value, out var iBase))
            {
                iBase.Value = FsmVariable.GetValue();
            } 
        }

#if UNITY_EDITOR
        private float lineHeight => EditorGUIUtility.singleLineHeight;
        private float spacing => EditorGUIUtility.standardVerticalSpacing;
        public float GetPropertyHeight()
        {
            return (lineHeight + spacing) * 2f;
        }
        public void PropertyField(Rect position, out Vector2 pos, out Vector2 size)
        {
            pos = position.position;
            size = new Vector2(position.width, lineHeight);
            {
                var rect = new Rect(pos, size);
                var content = ObjectNames.NicifyVariableName(nameof(VariableName));
                VariableName.Value =
                    EditorGUI.TextField(rect, content, VariableName.Value);
            }
            pos.y += size.y + spacing;
            {
                var rect = new Rect(pos, size);
                var content = ObjectNames.NicifyVariableName(nameof(FsmVariable));
                FsmVariable.variableName =
                    EditorGUI.TextField(rect, content, FsmVariable.variableName);
            }
        }
#endif
    }
}