using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
using UnityEditor; 
#endif

namespace SimpleVariables
{
    [Serializable]
    public class LiteralData : IDisposable
    {
        [SerializeField]
        public Object WithObject = null;
        [TextArea]
        [SerializeField]
        public string Literal = null;
        [SerializeField]
        public EnvironmentReferencingEnum Referencing = EnvironmentReferencingEnum.Asset;
        [SerializeField]
        public VariableAsset ReferencedAsset = null;
        [SerializeField]
        public VariableComponent ReferencedComponent = null;

        public LiteralData()
        {
        }
        public LiteralData(Object withObject)
        {
            WithObject = withObject;
        }
        public LiteralData(Object withObject, string literal, IVariableEnvironment environment)
        {
            WithObject = withObject;
            Literal = literal;
            Environment = environment;
        }

        protected DelegateKey key = null;
        protected DelegateData data = null;
        protected int invokedFrame = -1;

        public IVariableEnvironment Environment
        {
            get
            {
                switch (Referencing)
                {
                    case EnvironmentReferencingEnum.Asset:
                        return ReferencedAsset;
                    case EnvironmentReferencingEnum.Component:
                        return ReferencedComponent;
                }

                throw new InvalidOperationException($"The property {nameof(Referencing)} has invalid value!");
            }
            set
            {
                if (value is VariableAsset)
                    Referencing = EnvironmentReferencingEnum.Asset;
                else
                    Referencing = EnvironmentReferencingEnum.Component;

                switch (Referencing)
                {
                    case EnvironmentReferencingEnum.Asset:
                        ReferencedAsset = (VariableAsset)value;
                        break;
                    case EnvironmentReferencingEnum.Component:
                        ReferencedComponent = (VariableComponent)value;
                        break;
                    default:
                        throw new InvalidOperationException();
                }
            }
        }
        public DelegateData Data
        {
            get
            {
                return data;
            }
        }

        public void Compile(string literal, IVariableEnvironment env)
        {
            /// It was compiled before.
            if (key is object)
            {
                bool same = this.Literal == literal && Environment == env;
                if (same)
                {
                    foreach (var used in this.data.Variables)
                    {
                        used.AddUsingObject(WithObject);
                    }
                    return;
                }
                else
                {
                    literal = this.Literal;
                    Environment = env;
                    foreach (var used in this.data.Variables)
                    {
                        used.RemoveUsingObject(WithObject);
                    }
                }
            }

            key = new DelegateKey(this.Literal, typeof(Action), Environment);
            if (DelegateManager.Pairs.TryGetValue(key, out var data))
            {
                this.data = data;
                this.data.Owners.Add(this);
            }
            else
            {
                var action = ExpressionUtility.Compile<Action>(this.Literal, out var usedVariables, Environment);
                this.data = new DelegateData(action, usedVariables);
                this.data.Owners.Add(this);
                DelegateManager.Pairs.Add(key, this.data);
            }

            foreach (var used in this.data.Variables)
            {
                used.AddUsingObject(WithObject);
            }
        }
        public void Invoke()
        {
            Data.Action.Invoke();
#if UNITY_EDITOR
            Data.Use();
#endif            
        }
        public void OnInit()
        {
            if (string.IsNullOrEmpty(Literal))
                return;
            Compile(Literal, Environment);
        }
        public void OnDispose()
        {
            if (data is object)
            {
                foreach (var variable in data.Variables)
                    variable.RemoveUsingObject(WithObject);
            }
            DelegateManager.Purge();
        }
        public void Dispose()
        {
            OnDispose();
        }

#if UNITY_EDITOR
        public void TestCompile()
        {
            ExpressionUtility.Compile<Action>(Literal, out _, Environment);
        }

        public void PropertyLayout(SerializedProperty prop, bool editable)
        {
            var serializedObject = prop.serializedObject;
            serializedObject.Update();
            EditorGUI.BeginDisabledGroup(!editable);
            {
                var referencingProp = prop.FindPropertyRelative(nameof(Referencing));
                var referencedAssetProp = prop.FindPropertyRelative(nameof(ReferencedAsset));
                var referencedComponentProp = prop.FindPropertyRelative(nameof(ReferencedComponent));
                EditorGUILayout.PropertyField(referencingProp);
                EditorGUI.BeginDisabledGroup(referencingProp.enumValueIndex != 0);
                EditorGUILayout.PropertyField(referencedAssetProp);
                EditorGUI.EndDisabledGroup();
                EditorGUI.BeginDisabledGroup(referencingProp.enumValueIndex != 1);
                EditorGUILayout.PropertyField(referencedComponentProp);
                EditorGUI.EndDisabledGroup();
            }
            {
                var literalProp = prop.FindPropertyRelative(nameof(Literal));
                var rect = EditorGUILayout.GetControlRect();
                var content = new GUIContent(literalProp.displayName);
                EditorGUI.BeginProperty(rect, content, literalProp);
                EditorGUI.LabelField(rect, content);
                EditorGUI.EndProperty();
                CustomEditorGUIUtility.TextArea(literalProp);
            }
            serializedObject.ApplyModifiedProperties();
            EditorGUI.EndDisabledGroup();
        }
#endif
    }
}