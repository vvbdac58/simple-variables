﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleVariables
{
    public class DelegateData : IDisposable
    {
        public Action Action = null;
        public List<BaseVariable> Variables = new List<BaseVariable>();
        public List<LiteralData> Owners = new List<LiteralData>();

        public DelegateData(Action action, List<BaseVariable> variables)
        {
            Action = action;
            Variables = variables;
        }

        public void AddUsingObject(UnityEngine.Object obj)
        {
            foreach (var used in Variables)
                used.AddUsingObject(obj);
        }
        public void RemoveUsingObject(UnityEngine.Object obj)
        {
            foreach (var used in Variables)
                used.RemoveUsingObject(obj);
        }
        public void Use()
        {
            foreach (var used in Variables)
                used.UsedFrame = Time.frameCount;
        } 

        public void Dispose()
        {
            Action = null;
            Variables.Clear();
            Owners.Clear();
        }
    }
}