﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;

namespace SimpleVariables
{
    public class ExpressionNode
    {
        public string Text;
        public Expression Parsed;
        public LambdaExpression[] Arguements;

        public bool IsClassName;
        public Type ClassType;

        public bool HasDepth;
        public bool IsParameters;
        public int Depth;
        public List<ExpressionNode> DeeperNodes;

        public readonly bool isOperator;
        public readonly int priority;
        public readonly OperatorData operatorData;

        public readonly bool isLexeme;
        public readonly LexemeData lexemeData;
        
        public readonly bool isConstant;
        public readonly bool isString;
        public readonly bool isMethod;
        public readonly bool isIndexer;

        /// <summary>
        /// Ctor for constant and string
        /// </summary>
        /// <param name="literal"> text part of it </param>
        /// <param name="depth"> depth of its position </param>
        public ExpressionNode(string literal, int depth, bool isMethod, bool isIndexer)
        {
            Text = literal;
            Depth = depth;

            isLexeme = LiteralUtility.IsLexeme(literal);
            isConstant = LiteralUtility.IsConstant(literal);
            isOperator = OperatorUtility.IsOperator(literal);
            isString = LiteralUtility.IsString(literal);
            this.isMethod = isMethod;
            this.isIndexer = isIndexer;

            if (isLexeme || isConstant || isString)
                priority = OperatorUtility.MinPriority;

            DeeperNodes = null;
        }
        /// <summary>
        /// Contrusts a deeper node
        /// </summary>
        /// <param name="literal"> text of the node </param>
        /// <param name="depth"> depth of its position </param>
        /// <param name="nodes"> nodes to store </param>
        public ExpressionNode(List<ExpressionNode> nodes, bool isParameters, int depth)
        {
            Text = nodes.ConvertToString();
            Depth = depth;
            HasDepth = true;

            IsParameters = isParameters;

            isLexeme = false;
            isConstant = false;
            isOperator = false;
            isString = false;
            priority = OperatorUtility.MinPriority;

            DeeperNodes = nodes;

            operatorData = new OperatorData(true);
            lexemeData = new LexemeData(true);
        }
        /// <summary>
        /// Contrusts a node for operator.
        /// </summary>
        /// <param name="literal"> text of the operator </param>
        /// <param name="unary"> is operator unary? </param>
        /// <param name="depth"> depth of its position </param>
        public ExpressionNode(string literal, bool unary, int depth) :
            this(literal, depth, false, false)
        {
            operatorData = OperatorUtility.GetOperatorData(literal, unary);
            priority = operatorData.prior;

            lexemeData = new LexemeData(true);
        }
        /// <summary>
        /// Contrusts a node for lexeme.
        /// </summary>
        /// <param name="literal"> text part of the lexeme </param>
        /// <param name="isHead"> is the lexeme the first one to access? </param>
        /// <param name="isMethod"> the the lexeme a method? </param>
        /// <param name="depth"> depth of its position </param>
        public ExpressionNode(string literal, bool isHead, bool isMethod, bool isIndexer, int depth) :
            this(literal, depth, isMethod, isIndexer)
        {
            operatorData = new OperatorData(true);
            priority = OperatorUtility.MinPriority;

            lexemeData = new LexemeData(isHead, isMethod);
        }

        public override string ToString()
        {
            return Text;
        }
        public string Log()
        {
            if (isOperator)
                return $"oper:[{ToString()}, unary {operatorData.unary}]";
            else if (isConstant)
                return $"constant:{ToString()}";
            else if (isLexeme)
                return $"lexeme:[{ToString()}, var {lexemeData.isVariable}]";
            else if (isString)
                return $"str:{ToString()}";
            else if (HasDepth)
            {
                StringBuilder build = new StringBuilder("(");
                foreach (var node in DeeperNodes)
                    build.Append($"{node.Log()}, ");
                build.Remove(build.Length - 2, 2);
                build.Append(")");
                return build.ToString();
            }
            else
                return $"unexpected:{ToString()}";
        }
    }
}