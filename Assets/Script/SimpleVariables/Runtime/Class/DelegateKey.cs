﻿using System;

namespace SimpleVariables
{
    public class DelegateKey : IEquatable<DelegateKey>
    {
        public readonly string literal;
        public readonly Type delegateType;
        public readonly IVariableEnvironment environment;

        public DelegateKey(string literal, Type delegateType, IVariableEnvironment environment)
        {
            this.literal = literal;
            this.delegateType = delegateType;
            this.environment = environment;
        }

        public bool Equals(DelegateKey other)
        {
            return GetHashCode() == other.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(literal, delegateType, environment);
        }

        public override string ToString()
        {
            return literal;
        }
    }
}