using System;

namespace SimpleVariables
{
    public class CustomVariableAttribute : Attribute
    {
        public Type TargetType;
        public string ValueFieldName;
        public string MenuPath;
        public CustomVariableContainerEnum Container;

        public CustomVariableAttribute(Type type, string valueFieldName)
            : this(type, valueFieldName, valueFieldName, CustomVariableContainerEnum.NotContainer)
        {
        }
        public CustomVariableAttribute(Type type, string valueFieldName, string menuPath)
            : this(type, valueFieldName, menuPath, CustomVariableContainerEnum.NotContainer)
        {
        }
        public CustomVariableAttribute(Type type, string valueFieldName, CustomVariableContainerEnum container)
            : this(type, valueFieldName, valueFieldName, container)
        {
        }
        public CustomVariableAttribute(Type type, string valueFieldName, string menuPath, CustomVariableContainerEnum container)
        {
            TargetType = type;
            ValueFieldName = valueFieldName;
            MenuPath = menuPath;
            Container = container;
        }
    }
}