using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleVariables
{
    public interface IVariableEnvironment : IEnumerable<BaseVariable>, IName
    {
    }
}