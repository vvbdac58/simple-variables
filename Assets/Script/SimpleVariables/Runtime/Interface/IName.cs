using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleVariables
{
    /// <summary>
    /// This interface can helps us to name and get name of every variable, asset and variables.
    /// <br> When we want to know wich variable expression is referencing, this interface comes in handy. </br>
    /// </summary>
    public interface IName
    {
        /// <summary>
        /// Gets the name of the class.
        /// </summary>
        /// <returns> name of the class. </returns>
        string GetName();
    }
}