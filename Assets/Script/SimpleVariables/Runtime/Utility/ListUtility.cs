using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace SimpleVariables
{
    public static class ListUtility
    {
        public static string ConvertToString<T>(this List<T> list)
        {
            StringBuilder build = new StringBuilder();
            foreach (var item in list)
                build.Append(item.ToString());
            return build.ToString();
        }
        public static void RemovePick<T>(this List<T> list, params int[] indices)
        {
            int removed = 0;
            foreach (var index in indices)
            {
                list.RemoveAt(index - removed);
                removed += 1;
            }
        }
        public static List<List<T>> Splits<T>(this List<T> list, Func<T, bool> predicate, bool withEmptyList = false)
        {
            List<List<T>> splits = new List<List<T>>();
            List<T> current = new List<T>();
            foreach (var item in list)
            {
                if (predicate.Invoke(item))
                {
                    if (withEmptyList)
                    {
                        splits.Add(current);
                        current = new List<T>();
                    }
                    else if (current.Count > 0)
                    {
                        splits.Add(current);
                        current = new List<T>();
                    }
                }
                else
                {
                    current.Add(item);
                }
            }
            if (withEmptyList)
            {
                splits.Add(current);
            }
            else if (current.Count > 0)
            {
                splits.Add(current);
            }
            return splits;
        }
        public static List<T> Sublist<T>(this List<T> list, int index, int count, bool remove = true)
        {
            List<T> sub = new List<T>();
            for (int j = 0; j < count && j + index < list.Count; j++)
                sub.Add(list[j + index]);
            if (remove)
                list.RemoveRange(index, count);
            return sub;
        }
    }
}