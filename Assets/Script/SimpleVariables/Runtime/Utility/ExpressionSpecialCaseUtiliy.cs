using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using static SimpleVariables.ExpressionUtility;

namespace SimpleVariables
{
    public static class ExpressionSpecialCaseUtiliy
    {
        #region Compile
        /// <summary>
        /// Parses and compiles the literal without telling the certain type.
        /// </summary>
        /// <param name="literal"> the multi-line raw literal to input that is ready to parse </param>
        /// <param name="env"> the environment containing variable for parsing </param>
        /// <param name="delegateType"> type of the parsed delegate </param>
        /// <returns> the parsed delegate </returns>
        public static Delegate Compile(string literal, IVariableEnvironment env, out Type delegateType)
        {
            delegateType = GetDelegateType(literal, env, out var lambda);
            return lambda.Compile();
        }
        /// <summary>
        /// Gets the delegate type of literal
        /// </summary>
        /// <param name="literal"> the multi-line raw literal to input that is ready to parse </param>
        /// <param name="env"> the environment containing variable for parsing </param>
        /// <param name="lambda"> the lambda expression which was parsed in the function </param>
        /// <returns> type of the delegate </returns>
        public static Type GetDelegateType(string literal, IVariableEnvironment env, out LambdaExpression lambda)
        {
            /// dealing with multi-line
            var expression = NewLineMatch.Split(literal)
                .Select(s => ToExpression(ToNodes(ToStrings(s), 0, null, env), env)); /// parses every line
            var block = Expression.Block(expression); /// combing them together
            lambda = Expression.Lambda(block);
            var returnType = lambda.ReturnType;
            var returnTypes = new Type[] { returnType };
            if (Expression.TryGetActionType(returnTypes, out Type type))
                return type;
            else if (Expression.TryGetFuncType(returnTypes, out type))
                return type;
            else
                throw new InvalidOperationException();
        }
        #endregion
    }
}