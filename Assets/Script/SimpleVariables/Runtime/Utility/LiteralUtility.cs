using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace SimpleVariables
{
    public static class LiteralUtility
    {
        public static readonly Regex NotValidMatch
            = new Regex("[!@#$%^&*()\\-=+\\[\\]{}|\\\\:;\'\"<>/?.\a\b\n\r`~]");
        public static readonly Regex DigitMatch
            = new Regex("[0-9]");
        public static readonly Regex AlphabetMatch
            = new Regex("[a-zA-Z0-9]");
        public static readonly Regex FloatingMarkMatch
            = new Regex("[fFDdMm]");
        public static bool IsLexeme(string literal)
        {
            if (string.IsNullOrEmpty(literal))
                throw new ArgumentNullException();

            literal = literal.Trim();
            if (literal.Length == 0)
                return false;
            if (char.IsDigit(literal[0]))
                return false;

            if (IsConstant(literal))
                return false;

            if (NotValidMatch.IsMatch(literal))
                return false;
            else
                return true;
        }
        public static bool IsConstant(string literal)
        {
            if (string.IsNullOrEmpty(literal))
                throw new ArgumentNullException();

            if (literal == ".")
                return false;

            literal = literal.Trim();
            if (literal.Length == 0)
                return false;
            if (literal.Count(c => c == '.') > 1)
                return false;

            literal = literal.Replace(".", string.Empty);
            literal = literal.Replace("_", string.Empty);

            if (literal == "true" || literal == "false")
            {
                return true;
            }
            else if (literal.StartsWith("0x") || literal.StartsWith("0X"))
            { 
                literal = literal.Remove(0, 2);

                literal = AlphabetMatch.Replace(literal, string.Empty);
            }
            else
            {
                if (literal.Length > 1)
                {
                    var lastCharacter = literal[literal.Length - 1] + "";
                    if (FloatingMarkMatch.IsMatch(lastCharacter))
                        literal = literal.Remove(literal.Length - 1, 1);
                }
                literal = DigitMatch.Replace(literal, string.Empty);
            }

            if (literal.Length > 0)
                return false;
            else
                return true;
        }
        public static bool IsString(string literal)
        {
            if (string.IsNullOrEmpty(literal))
                throw new ArgumentNullException();

            literal = literal.TrimStart().TrimStart();
            if (literal.Length >= 2)
                return (literal[0] == '\"' && literal[literal.Length - 1] == '\"') ||
                    (literal[0] == '\'' && literal[literal.Length - 1] == '\'');
            else
                return false;
        }
        public static bool IsDepthOrderer(string literal, out OrdererData data)
        {
            if (string.IsNullOrEmpty(literal))
                throw new ArgumentNullException();

            literal = literal.Trim();

            if (literal.Length == 1)
            {
                data = new OrdererData(literal[0]);
                return !data.isInvalid;
            }
            else
            {
                data = new OrdererData('\0');
                return false;
            }
        }
        public static object ConvertLiteral(string literal)
        {
            var clear = literal.Trim();
            var formatted = clear;
            try
            {
                {
                    if (literal[0] == '\"' && literal[literal.Length - 1] == '\"' ||
                        literal[0] == '\'' && literal[literal.Length - 1] == '\'')
                    {
                        if (literal.Length > 2)
                            return literal.Substring(1, literal.Length - 2);
                        else
                            return string.Empty;
                    }
                    else if (literal == "true")
                    {
                        return true;
                    }
                    else if (literal == "false")
                    {
                        return false;
                    }
                    else if (literal.Contains(".") || char.IsLetter(clear[clear.Length - 1]))
                    {
                        if (char.IsLetter(clear[clear.Length - 1]))
                            formatted = clear.Remove(clear.Length - 1, 1);

                        if (clear.EndsWith("f") || clear.EndsWith("F"))
                            return Convert.ToSingle(formatted);
                        else if (clear.EndsWith("d") || clear.EndsWith("D"))
                            return Convert.ToDouble(formatted);
                        else if (clear.EndsWith("m") || clear.EndsWith("M"))
                            return Convert.ToDecimal(formatted);
                        else
                            return Convert.ToSingle(clear);
                    }
                    else if (clear.StartsWith("0x") || clear.StartsWith("0X"))
                    {
                        //if (clear.StartsWith("0x") || clear.StartsWith("0X"))
                        return Convert.ToInt32(clear, 16);
                        //else if (clear.StartsWith("0b") || clear.StartsWith("0B"))
                        //    return Convert.ToInt32(clear, 2);
                        //else
                        //    return Convert.ToInt32(clear);
                    }
                    else
                    {
                        return Convert.ToInt32(clear);
                    }
                }
            }
            catch
            {
                return literal;
            }
        }
    }
}