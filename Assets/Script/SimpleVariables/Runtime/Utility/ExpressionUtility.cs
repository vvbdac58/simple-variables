using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using static SimpleVariables.ExpressionSpecialCaseUtiliy;

namespace SimpleVariables
{
    /// <summary>
    /// Utilies for expression.
    /// <br> The most complicated utiliy in namespace SimpleVariables. </br>
    /// </summary>
    public static class ExpressionUtility
    {
        #region Compiling
        /// <summary>
        /// Parses and compiles the literal into a certain type of delegate.
        /// </summary>
        /// <typeparam name="T"> the designated type of delegate </typeparam>
        /// <param name="literal"> the multi-line raw literal to input that is ready to parse </param>
        /// <param name="env"> the environment containing variable for parsing </param>
        /// <returns> the delegate of a generic given type </returns>
        public static T Compile<T>(string literal, out List<BaseVariable> usedVariables, IVariableEnvironment env) where T : Delegate
        {
            /// recording the variables been used.
            usedVariables = new List<BaseVariable>();
            /// turning into a bock {}
            var block = ToBlock(literal, usedVariables, env);
            /// compile the block finally
            return (T)Expression.Lambda(typeof(T), block).Compile();
        }
        /// <summary>
        /// Combes all multi-line literal into a block.
        /// </summary>
        /// <param name="literal"> the multi-line raw literal to input that is ready to parse </param>
        /// <param name="env"> the environment containing variable for parsing </param>
        /// <returns> blocks of expression </returns>
        private static BlockExpression ToBlock(string literal, List<BaseVariable> usedVariables, IVariableEnvironment env)
        {
            /// dealing with multi-line
            var expression = NewLineMatch.Split(literal)
                .Select(s => ToExpression(ToNodes(ToStrings(s), 0, usedVariables, env), env)); /// parses every line
            /// combing them together
            return Expression.Block(expression);
        }
        #endregion

        #region ToStrings

        /// <summary>
        /// Used to split multi-line into list.
        /// </summary>
        public static readonly Regex NewLineMatch = new Regex("\\n", RegexOptions.Multiline);

        /// <summary>
        /// Used to recognize every element in expression, but not operators.
        /// <br> We seperated every operator's Regex in OperatorData in OperatorUtility.Datas </br>
        /// </summary>
        private static readonly Regex[] Matches = new Regex[]
        {
            new Regex("([\"\'])([^\\\\]*?(?:\\\\.[^\\\\]*?)*)([\"\'])"), // String also counts escape " in
            new Regex("0[xX][0-9a-zA-Z]+"), // HexInteger
            new Regex("(?!_)[0-9_]*[.][0-9_]+[fFDdMm]?|^(?!_)[0-9_]+[fFDdMm]?"), // FloatingPoints
            new Regex("(?!_)[0-9_]+"), // Integer
            new Regex("[^ !@#$%^&*()\\-=+\\[\\]{}|\\\\:;\'\"<>/?.,\a\b\n\r`~]+"), // Lexeme
            new Regex("[.]"), // Dot
            new Regex("[()\\[\\]{}]"), // Orderer
            new Regex("[ ]+"), // Spaces
            new Regex("[\\n]"), // Newlines
        };
        /// <summary>
        /// Consists of Matches and OperatorDatas' regex, one for all regex.
        /// </summary>
        public static Regex Seperator
        {
            get
            {
                StringBuilder build = new StringBuilder();
                /// Combines every elements that recognizes quoted string, floating point, integer, constant and lexeme
                {
                    foreach (var m in Matches)
                        build.Append($"{m}|");
                }
                /// Combines every regex inside operators' datas
                {
                    var datas = OperatorUtility.Datas.OrderByDescending(d => d.text.Length);
                    foreach (var data in datas)
                        build.Append($"{data.regex}|");
                }
                /// Removes the last | symbol
                build.Remove(build.Length - 1, 1);

                return new Regex(build.ToString(), RegexOptions.Multiline);
            }
        }
        /// <summary>
        /// Step 1 for splitting expression to recognizable strings.
        /// </summary>
        /// <param name="expression"> input single line expression </param>
        /// <returns> list of recognizable strings </returns>
        public static List<string> ToStrings(string expression)
        {
            if (string.IsNullOrEmpty(expression))
                throw new ArgumentNullException($"The {nameof(expression)} is null!");

            /// Splits the expression.
            List<string> splits = new List<string>();
            var matches = Seperator.Match(expression);
            for (var m = matches; m.Success; m = m.NextMatch())
            {
                if (!string.IsNullOrEmpty(m.Value))
                    splits.Add(m.Value);
            }

            return splits;
        }
        #endregion

        #region ToNodes
        /// <summary>
        /// Step 2 for the processor to get contextful data. 
        /// </summary>
        /// <param name="expression"> The original expression to put in. </param>
        /// <returns> A list that splits out all string into meaningful data to processor. </returns>
        public static List<ExpressionNode> ToNodes(
            List<string> splits, int depth, 
            List<BaseVariable> usedVariables,
            IVariableEnvironment env)
        {
            if (splits is null)
                throw new ArgumentNullException($"Value {nameof(splits)} can not be null!");

            splits.RemoveAll(s => string.IsNullOrEmpty(s.Trim()));

            /// Turns all splits to nodes
            List<ExpressionNode> nodes = new List<ExpressionNode>();
            for (int i = 0; i < splits.Count; i++)
            {
                var literal = splits[i];

                bool isMethod = false;
                bool isIndexer = false;
                if (i < splits.Count - 1)
                {
                    var rightLiteral = splits[i + 1];
                    if (LiteralUtility.IsDepthOrderer(rightLiteral, out OrdererData data))
                    {
                        isMethod = data.isParameters && !data.isIndexer;
                        isIndexer = data.isIndexer;
                    }
                }

                if (LiteralUtility.IsDepthOrderer(literal, out OrdererData orderer))
                {
                    int j = 1;
                    int count = 1;
                    for (; i + j < splits.Count; j++)
                    {
                        literal = splits[i + j];
                        if (LiteralUtility.IsDepthOrderer(literal, out OrdererData otherOrderer))
                        {
                            if (otherOrderer.isOpenning)
                                count += 1;
                            if (otherOrderer.isClosing)
                                count -= 1;
                            if (count == 0 && orderer.IsPair(otherOrderer))
                            {
                                j += 1;
                                break;
                            }
                        }
                    }

                    /// Gets out the list in quote.
                    var deeperSplits = splits.Sublist(i, j, true);
                    /// Removes fore-quote.
                    deeperSplits.RemoveAt(0);
                    /// Removes back-quote.
                    deeperSplits.RemoveAt(deeperSplits.Count - 1);
                    /// Passing the node before is method and this should be paramters.
                    bool isParameters = nodes[nodes.Count - 1].isLexeme;
                    /// Parses the deeper splits.
                    nodes.Add(
                        new ExpressionNode(
                            ToNodes(deeperSplits, depth + 1, usedVariables, env),isParameters, depth + 1));
                }
                else if (OperatorUtility.IsOperator(literal))
                {
                    if (literal == ".")
                        continue;

                    bool unary = false;
                    if (i == 0)
                    {
                        unary = true;
                    }
                    else
                    {
                        try
                        {
                            var leftNode = nodes[i - 1];
                            if (leftNode.isOperator)
                                unary = true;
                        }
                        catch
                        {
                            throw new ArithmeticException();
                        }
                    }
                    nodes.Add(new ExpressionNode(literal, unary, depth));
                }
                else if (LiteralUtility.IsLexeme(literal))
                {
                    bool isHead = true;
                    if (i > 0)
                    {
                        var leftLiteral = splits[i - 1];
                        if (leftLiteral == ".")
                            isHead = false;
                    }

                    if (isHead)
                    {
                        if (env.TryGetVariable(literal, out var variable))
                        {
                            nodes.Add(new ExpressionNode(literal, isHead, isMethod, isIndexer, depth));
                            if (usedVariables is object)
                                usedVariables.Add(variable);
                        }
                        else
                        {
                            throw new KeyNotFoundException($"Can't find variable {literal} in {env.GetName()}!");
                        }
                    }
                    else
                    {
                        nodes.Add(new ExpressionNode(literal, isHead, isMethod, isIndexer, depth));
                    }
                }
                else
                {
                    nodes.Add(new ExpressionNode(literal, depth, isMethod, isIndexer));
                }
            }

            nodes.RemoveAll(n => n.Text == ".");
            return nodes;
        }
        #endregion

        #region ToExpression
        /// <summary>
        /// This one is hand-crafted for we can't figure it out which type is our target.
        /// </summary>
        /// <param name="a"> type on the left </param>
        /// <param name="b"> type on the right </param>
        /// <param name="wantConvertRight"> target is left </param>
        /// <returns> the target type </returns>
        private static Type GetTargetType(Type a, Type b, out bool wantConvertRight)
        {
            Type result = a;
            ///object & !object
            if (a == typeof(object) && b != typeof(object))
                result = b;
            else if (a != typeof(object) && b == typeof(object))
                result = a;
            ///int & float
            else if(a == typeof(int) && b == typeof(float))
                result = typeof(float);
            else if (a == typeof(float) && b == typeof(int))
                result = typeof(float);
            ///int & double
            else if (a == typeof(int) && b == typeof(double))
                result = typeof(double);
            else if (a == typeof(double) && b == typeof(int))
                result = typeof(double);
            ///int & decimal
            else if (a == typeof(int) && b == typeof(decimal))
                result = typeof(decimal);
            else if (a == typeof(decimal) && b == typeof(int))
                result = typeof(decimal);
            ///string
            else if (a == typeof(string) || b == typeof(string))
                result = typeof(string);
            else
                throw new KeyNotFoundException($"comparing {a} and {b} is not implemented!");
            wantConvertRight = result == a;
            return result;
        }
        /// <summary>
        /// Makes both return type the same.
        /// </summary>
        /// <param name="left"> expression on the left </param>
        /// <param name="right"> expression on the right </param>
        /// <returns> returns the target type comparing both </returns>
        private static Type SyncTypes(ref Expression left, ref Expression right)
        {
            var leftLambda = Expression.Lambda(left);
            var rightLambda = Expression.Lambda(right);
            var leftType = leftLambda.ReturnType;
            var rightType = rightLambda.ReturnType;
            if (leftType != rightType)
            {
                var target = GetTargetType(leftType, rightType, out bool wantConvertRight);
                if (target == typeof(string))
                {
                    if (wantConvertRight)
                    {
                        TypeUtility.TryGetMethod(rightType, "ToString",
                            new Type[0], out var method);
                        right = Expression.Call(right, method);
                    }
                    else
                    {
                        TypeUtility.TryGetMethod(leftType, "ToString",
                            new Type[0], out var method);
                        left = Expression.Call(left, method);
                    }
                }
                else
                {
                    try
                    {
                        if (wantConvertRight)
                            right = Expression.ConvertChecked(right, target);
                        else
                            left = Expression.ConvertChecked(left, target);
                    }
                    catch (Exception ex)
                    {
                        if (wantConvertRight)
                            throw new Exception($"Can't convert from {right} to {target}!", ex);
                        else
                            throw new Exception($"Can't convert from {left} to {target}!", ex);
                    }
                }

                return target;
            }
            else
            {
                return leftType;
            }
        }
        /// <summary>
        /// Switches the statements for parsing into binary expression.
        /// <br> Making it a function will be easy for us to add new things inside. </br>
        /// <br> We have so many binary operators here... </br>
        /// </summary>
        /// <param name="name"> name of the operator </param>
        /// <param name="left"> the element connected to the left </param>
        /// <param name="right"> the element connected to the right </param>
        /// <returns> results to be used in Step 2 (ToLambda).</returns>
        private static Expression ParseBinaryNode(OperaterName name, Expression left, Expression right)
        {
            try
            {
                var target = SyncTypes(ref left, ref right);

                switch (name)
                {
                    case OperaterName.Multiply:
                        return Expression.Multiply(left, right);
                    case OperaterName.Division:
                        return Expression.Divide(left, right);
                    case OperaterName.Modulus:
                        return Expression.Modulo(left, right);
                    case OperaterName.Addition:
                        if (target != typeof(string))
                        {
                            return Expression.Add(left, right);
                        }
                        else
                        {
                            TypeUtility.TryGetMethod(typeof(string), nameof(string.Concat),
                                new Type[] { typeof(string), typeof(string) }, out var method);
                            return Expression.Call(null, method, left, right);
                        }
                    case OperaterName.Subtraction:
                        return Expression.Subtract(left, right);
                    case OperaterName.LeftShift:
                        return Expression.LeftShift(left, right);
                    case OperaterName.RightShift:
                        return Expression.RightShift(left, right);
                    case OperaterName.GreaterThan:
                        return Expression.GreaterThan(left, right);
                    case OperaterName.LessThan:
                        return Expression.LessThan(left, right);
                    case OperaterName.GreaterThanOrEqual:
                        return Expression.GreaterThanOrEqual(left, right);
                    case OperaterName.LessThanOrEqual:
                        return Expression.LessThanOrEqual(left, right);
                    case OperaterName.Equality:
                        return Expression.Equal(left, right);
                    case OperaterName.Inequality:
                        return Expression.NotEqual(left, right);
                    case OperaterName.BitwiseAnd:
                        return Expression.And(left, right);
                    case OperaterName.BitwiseOr:
                        return Expression.Or(left, right);
                    case OperaterName.LogicAnd:
                        return Expression.And(left, right);
                    case OperaterName.LogicOr:
                        return Expression.Or(left, right);
                    default:
                        throw new InvalidOperationException();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Can't do {left} {name} {right}!", ex);
            }
        }
        /// <summary>
        /// Switches the statements for parsing into unary expression.
        /// <br> Making it a function will be easy for us to add new things inside. </br>
        /// </summary>
        /// <param name="name"> name of the operator </param>
        /// <param name="right"> the element connected to the right </param>
        /// <returns> results to be used in Step 2 (ToLambda).</returns>
        private static UnaryExpression ParseUnaryNode(OperaterName name, Expression right)
        {
            switch (name)
            {
                case OperaterName.Not:
                    return Expression.Not(right);
                case OperaterName.Negate:
                    return Expression.Negate(right);
                default:
                    throw new InvalidOperationException();
            }
        }
        /// <summary>
        /// Matches method for provided name and arguements.
        /// <br> This is the hardest part of it! </br>
        /// <br> Please watch out for ambiguously calling, it will match the first valid one. </br>
        /// </summary>
        /// <returns> the method that is valid </returns>
        private static bool TryMatchMethod(Type type, string name, LambdaExpression[] args, out MethodInfo method)
        {
            var argTypes = args
                .Select(arg => arg.ReturnType)
                .ToArray();
            return TypeUtility.TryGetMethod(type, name, argTypes, out method);
        }
        /// <summary>
        /// Matches indexer for provided arguements.
        /// <br> Please watch out for ambiguously calling, it will match the first valid one. </br>
        /// </summary>
        /// <returns> the indexer that is valid </returns>
        private static bool TryMatchIndexer(Type type, LambdaExpression[] args, out PropertyInfo indexer)
        {
            var argTypes = args
                .Select(arg => arg.ReturnType)
                .ToArray();
            return TypeUtility.TryGetIndexer(type, argTypes, out indexer);
        }
        /// <summary>
        /// Parses the assign node for converting different data type together.
        /// </summary>
        /// <param name="left"> left expression </param>
        /// <param name="right"> right expression </param>
        /// <returns> the result of the assign expression </returns>
        private static BinaryExpression ParseAssignNode(OperaterName assignment, Expression left, Expression right)
        {
            var leftLambda = Expression.Lambda(left);
            var rightLambda = Expression.Lambda(right);
            var leftType = leftLambda.ReturnType;
            var rightType = rightLambda.ReturnType;
            if (leftType != rightType)
                right = Expression.Convert(right, leftType);
            switch (assignment)
            {
                case OperaterName.Assign:
                    return Expression.Assign(left, right);
                case OperaterName.AddAssign:
                    return Expression.AddAssignChecked(left, right);
                case OperaterName.SubtractAssign:
                    return Expression.SubtractAssignChecked(left, right);
                case OperaterName.MultiplyAssign:
                    return Expression.MultiplyAssignChecked(left, right);
                case OperaterName.DivideAssign:
                    return Expression.DivideAssign(left, right);
                case OperaterName.ModulosAssign:
                    return Expression.ModuloAssign(left, right);
                case OperaterName.AndAssign:
                    return Expression.AndAssign(left, right);
                case OperaterName.OrAssign:
                    return Expression.OrAssign(left, right);
                default:
                    throw new InvalidOperationException();
            }
        }
        /// <summary>
        /// Step 3 for the processor to combine every single expression together.
        /// </summary>
        /// <param name="nodes"> The data output by step 1 (ToNodes). </param>
        /// <returns> A lambda expression for dynamic invoking. </returns>
        public static Expression ToExpression(List<ExpressionNode> nodes, IVariableEnvironment env)
        {
            int maxDepth = nodes.Max(n => n.Depth);
            /// Stores the head of nodes,
            /// insuring the lambda knows where to start.
            Expression at = null;
            /// Start to parse all the nodes.
            /// Parse the nodes from deepest to shallowest
            for (int d = maxDepth; d >= 0; d--)
            {
                /// Parse the nodes from lowest priority to highest priority
                for (int p = -1; p <= OperatorUtility.MaxPriority; p++)
                {
                    /// Some of the operator functions from left to right as normal.
                    /// However, the operator of assignment works from right to left.
                    int i = OperatorUtility.IsL2R(p) ? 0 : nodes.Count - 1;
                    while (i >= 0 && i < nodes.Count)
                    {
                        var node = nodes[i];
                        /// Parse the correct depth.
                        if (node.Depth == d)
                        {
                            /// Switches out the priority.
                            /// Also use if statement is fine, but we used switch instead.
                            switch (p)
                            {
                                case OperatorUtility.MinPriority:
                                    if (node.priority == -1)
                                    {
                                        if (node.HasDepth)
                                        {
                                            if (node.IsParameters)
                                            {
                                                if (node.DeeperNodes.Count > 0)
                                                {
                                                    var argSplits = node.DeeperNodes.Splits(n => n.Text == ",");
                                                    var args = argSplits.Select(s => Expression.Lambda(ToExpression(s, env))).ToArray();
                                                    node.Arguements = args;
                                                    node.HasDepth = false;
                                                }
                                                else
                                                {
                                                    node.Arguements = new LambdaExpression[0];
                                                    node.HasDepth = false;
                                                }
                                            }
                                            else
                                            {
                                                node.Parsed = ToExpression(node.DeeperNodes, env);
                                                node.HasDepth = false;
                                                at = node.Parsed;
                                            }
                                        }
                                        else if (node.isConstant || node.isString)
                                        {
                                            var obj = LiteralUtility.ConvertLiteral(node.Text);
                                            node.Parsed = Expression.Constant(obj);
                                            at = node.Parsed;
                                        }
                                        else if (node.isLexeme)
                                        {
                                            if (node.lexemeData.isVariable)
                                            {
                                                if (env.TryGetVariable(node.Text, out var variable))
                                                {
                                                    /// Here the type of calling static class is handled.
                                                    if (variable is TypeVariable typeVar)
                                                    {
                                                        node.ClassType = (Type)typeVar.Value;
                                                        node.IsClassName = true;
                                                    }
                                                    /// Here the variable doesn't represent the type of the value.
                                                    /// It means the value is class and it is inheriting any class.
                                                    else if (variable.Value is object && variable.Value.GetType() != variable.ValueType)
                                                    {
                                                        var constant = Expression.Constant(variable.Value);
                                                        at = node.Parsed = constant;
                                                    }
                                                    /// Here the variable contains the same type of the value
                                                    else
                                                    {
                                                        var constant = Expression.Constant(variable);
                                                        var typeAs = Expression.TypeAs(constant, variable.GetType());
                                                        at = node.Parsed = Expression.PropertyOrField(typeAs, variable.ValueFieldName);
                                                    }
                                                }
                                                else
                                                {
                                                    throw new KeyNotFoundException($"Can't find variable {node.Text}!");
                                                }
                                            }
                                            else if (node.lexemeData.isFieldOrProperty)
                                            {
                                                if (nodes[i - 1].IsClassName)
                                                {
                                                    var left = nodes[i - 1].ClassType;
                                                    at = node.Parsed = Expression.Property(null, left, node.Text);
                                                    nodes.RemoveAt(i - 1);
                                                    i -= 1;
                                                }
                                                else
                                                {
                                                    var left = nodes[i - 1].Parsed;
                                                    at = node.Parsed = Expression.PropertyOrField(left, node.Text);
                                                    nodes.RemoveAt(i - 1);
                                                    i -= 1;
                                                }
                                            }

                                            if (node.isMethod)
                                            {
                                                if (nodes[i - 1].IsClassName)
                                                {
                                                    var left = nodes[i - 1].ClassType;
                                                    var args = nodes[i + 1].Arguements;
                                                    if (TryMatchMethod(left, node.Text, args, out MethodInfo method))
                                                    {
                                                        at = node.Parsed = Expression.Call(null, method, args.Select(arg => arg.Body));
                                                        nodes.RemovePick(i - 1, i + 1);
                                                        i -= 1;
                                                    }
                                                    else
                                                    {
                                                        throw new InvalidOperationException();
                                                    }
                                                }
                                                else
                                                {
                                                    var left = nodes[i - 1].Parsed;
                                                    var args = nodes[i + 1].Arguements;
                                                    if (TryMatchMethod(Expression.Lambda(left).ReturnType, node.Text, args, out MethodInfo method))
                                                    {
                                                        at = node.Parsed = Expression.Call(left, method, args.Select(arg => arg.Body));
                                                        nodes.RemovePick(i - 1, i + 1);
                                                        i -= 1;
                                                    }
                                                    else
                                                    {
                                                        throw new InvalidOperationException();
                                                    }
                                                }
                                            }
                                            if (node.isIndexer)
                                            {
                                                var type = Expression.Lambda(node.Parsed).ReturnType;
                                                var args = nodes[i + 1].Arguements;
                                                if (TryMatchIndexer(type, args, out PropertyInfo indexer))
                                                {
                                                    at = node.Parsed = Expression.Property(node.Parsed, indexer, args.Select(arg => arg.Body));
                                                    nodes.RemoveAt(i + 1);
                                                }
                                                else
                                                {
                                                    throw new InvalidOperationException();
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case 1:
                                    if (node.priority == p)
                                    {
                                        var right = nodes[i + 1].Parsed;
                                        at = node.Parsed = ParseUnaryNode(node.operatorData.name, right);
                                        nodes.RemoveAt(i + 1);
                                    }
                                    break;
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                    if (node.priority == p)
                                    {
                                        var left = nodes[i - 1].Parsed;
                                        var right = nodes[i + 1].Parsed;
                                        at = node.Parsed = ParseBinaryNode(node.operatorData.name, left, right);
                                        nodes.RemovePick(i - 1, i + 1);
                                        i -= 1;
                                    }
                                    break;
                                case 12:
                                    if (node.priority == p)
                                    {
                                        var left = nodes[i - 1].Parsed;
                                        var right = nodes[i + 1].Parsed;
                                        at = node.Parsed = ParseAssignNode(node.operatorData.name, left, right);
                                        nodes.RemovePick(i - 1, i + 1);
                                        i -= 1;
                                    }
                                    break;
                            }
                        }
                        /// Decides whether to move left or to move right.
                        i += OperatorUtility.IsL2R(p) ? 1 : -1;
                        /// There's a exception that when we input assignment into it.
                        /// The nodes' count will finally come to 1, because of count is odd number. 
                        /// And it must break or it won't have left and right to parse.
                        if (nodes.Count == 1 && nodes[0].Parsed is object)
                            break;
                    }
                }
            }

            return at;
        }
        #endregion
    }
}