﻿using System;
using System.Text.RegularExpressions;

namespace SimpleVariables
{
    public static class OperatorUtility
    {
        public const int MinPriority = -1;
        public const int MaxPriority = 12;

        public static bool IsL2R(int priority)
        {
            return priority < 12;
        }
        public static bool IsR2L(int priority)
        {
            return priority >= 12;
        }

        public static OperatorData[] Datas = new OperatorData[]
        {
            new OperatorData(false, true, 0, ".", new Regex("[.]"), OperaterName.Dot),
            new OperatorData(false, true, 0, ",", new Regex("[,]"), OperaterName.Comma),

            new OperatorData(true, false, 1, "-", new Regex("-"), OperaterName.Negate),
            new OperatorData(true, false, 1, "!", new Regex("!"), OperaterName.Not),

            new OperatorData(false, true, 2, "*", new Regex("[*]"), OperaterName.Multiply),
            new OperatorData(false, true, 2, "/", new Regex("/"), OperaterName.Division),
            new OperatorData(false, true, 2, "%", new Regex("%"), OperaterName.Modulus),

            new OperatorData(false, true, 3, "+", new Regex("[+]"), OperaterName.Addition),
            new OperatorData(false, true, 3, "-", new Regex("-"), OperaterName.Subtraction),

            new OperatorData(false, true, 4, "<<", new Regex("<<"), OperaterName.LeftShift),
            new OperatorData(false, true, 4, ">>", new Regex(">>"), OperaterName.RightShift),

            new OperatorData(false, true, 5, ">", new Regex(">"), OperaterName.GreaterThan),
            new OperatorData(false, true, 5, "<", new Regex("<"), OperaterName.LessThan),

            new OperatorData(false, true, 6, ">=", new Regex(">="), OperaterName.GreaterThanOrEqual),
            new OperatorData(false, true, 6, "<=", new Regex("<="), OperaterName.LessThanOrEqual),

            new OperatorData(false, true, 7, "==", new Regex("=="), OperaterName.Equality),
            new OperatorData(false, true, 7, "!=", new Regex("!="), OperaterName.Inequality),
            
            new OperatorData(false, true, 8, "&", new Regex("&"), OperaterName.BitwiseAnd),

            new OperatorData(false, true, 9, "|", new Regex("[|]"), OperaterName.BitwiseOr),

            new OperatorData(false, true, 10, "&&", new Regex("&&"), OperaterName.LogicAnd),

            new OperatorData(false, true, 11, "||", new Regex("[||]"), OperaterName.LogicOr),

            new OperatorData(false, true, 12, "=", new Regex("="), OperaterName.Assign),

            new OperatorData(false, true, 12, "!=", new Regex("[!]="), OperaterName.NotAssign),

            new OperatorData(false, true, 12, "+=", new Regex("[+]="), OperaterName.AddAssign),
            new OperatorData(false, true, 12, "-=", new Regex("[-]="), OperaterName.SubtractAssign),

            new OperatorData(false, true, 12, "*=", new Regex("[*]="), OperaterName.MultiplyAssign),
            new OperatorData(false, true, 12, "/=", new Regex("[/]="), OperaterName.DivideAssign),
            new OperatorData(false, true, 12, "%=", new Regex("[%]="), OperaterName.ModulosAssign),

            new OperatorData(false, true, 12, "&=", new Regex("[&]="), OperaterName.AndAssign),
            new OperatorData(false, true, 12, "|=", new Regex("[|]="), OperaterName.OrAssign),
        };
        public static bool IsOperator(string literal)
        {
            if (string.IsNullOrEmpty(literal))
                throw new ArgumentNullException();

            literal = literal.Trim();
            if (literal.Length == 0)
                return false;

            foreach (var data in Datas)
            {
                if (literal == data.text)
                    return true;
            }

            return false;
        }
        public static OperatorData GetOperatorData(string literal, bool unary)
        {
            if (!IsOperator(literal))
                return new OperatorData(true);

            foreach (var data in Datas)
            {
                if (literal == data.text && unary == data.unary)
                    return data;
            }

            return new OperatorData(true);
        }
    }
}