using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace SimpleVariables
{
    public static class TypeUtility
    {
        private static readonly BindingFlags Flags =
            BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.FlattenHierarchy;
        public static bool TryGetMethod(Type type, string name, Type[] arguments, out MethodInfo method, bool exactType = false)
        {
            var methods = type
                .GetMethods(Flags)
                .Where(m => m.Name == name)
                .ToArray();
            for (int i = 0; i < methods.Length; i++)
            {
                var parameters = methods[i].GetParameters().ToArray();
                if (MatchParameters(arguments, parameters, exactType))
                {
                    method = methods[i];
                    return true;
                }
            }
            method = null;
            return false;
        }
        public static bool TryGetIndexer(Type type, Type[] arguments, out PropertyInfo property, bool exactType = false)
        {
            var properties = type
                .GetProperties(Flags)
                .Where(p => p.GetIndexParameters().Length > 0)
                .ToArray();
            for (int i = 0; i < properties.Length; i++)
            {
                var parameters = properties[i].GetIndexParameters().ToArray();
                if (MatchParameters(arguments, parameters, exactType))
                {
                    property = properties[i];
                    return true;
                }
            }
            property = null;
            return false;
        }
        public static bool MatchParameters(Type[] arguments, ParameterInfo[] parameters, bool exactType = false)
        {
            int r = 0;
            int a = 0;
            for (; r < parameters.Length && a < arguments.Length;)
            {
                var remain = parameters[r];
                var argType = arguments[a];
                bool isParam = remain.GetCustomAttribute<ParamArrayAttribute>() is object;
                if (!exactType && remain.ParameterType.IsAssignableFrom(argType))
                {
                    if (!isParam)
                        r += 1;
                    a += 1;
                }
                else if (exactType && remain.ParameterType == argType)
                {
                    if (!isParam)
                        r += 1;
                    a += 1;
                }
                else
                {
                    break;
                }
            }
            if (a == arguments.Length)
            {
                if (r == parameters.Length)
                {
                    return true;
                }
                else
                {
                    var remain = parameters[r];
                    bool isParam = remain.GetCustomAttribute<ParamArrayAttribute>() is object;
                    bool isOptional = remain.IsOptional;
                    if (isParam || isOptional)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}