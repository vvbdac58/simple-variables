using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
#if PLAYMAKER
using HutongGames.PlayMaker;
#endif
#if UNITY_EDITOR
using UnityEditor; 
using UnityEditorInternal;
#endif

namespace SimpleVariables
{
    public static class VariableUtility
    {
#if UNITY_EDITOR
        public static void AllCustomVariableMenu(ReorderableList list, SerializedObject obj, List<BaseVariable> variables)
        {
            GenericMenu menu = new GenericMenu();
            foreach (var type in GetAllCustomVariableTypes())
            {
                menu.AddItem(
                    new GUIContent(GetMenuPath(type)),
                    false,
                    () =>
                    {
                        BaseVariable newVariable = (BaseVariable)Activator.CreateInstance(type);
                        if (list.index < 0 || list.index >= list.count - 1)
                            variables.Add(newVariable);
                        else
                            variables.Insert(list.index + 1, newVariable);
                        obj.ApplyModifiedProperties();
                    });
            }
            menu.ShowAsContext();
        } 
#endif
        public static Type[] GetAllCustomVariableTypes()
        {
            return AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(assem => assem.GetTypes())
                .Where(type => type.GetCustomAttribute<CustomVariableAttribute>() is object)
                .ToArray();
        }
        public static string[] GetAllVariablePaths(IVariableEnvironment env)
        {
            List<string> paths = new List<string>();

            getPaths(env, string.Empty);
            void getPaths(IVariableEnvironment _env, string parentPath)
            {
                foreach (var v in _env)
                {
                    if (string.IsNullOrEmpty(parentPath))
                        paths.Add($"{v.Name}");
                    else
                        paths.Add($"{parentPath}/{v.Name}");
                }
                foreach (var v in _env)
                {
                    if (v is IVariableEnvironment __env)
                    {
                        if (string.IsNullOrEmpty(parentPath))
                            parentPath = $"{v.Name}";
                        else
                            parentPath = $"{parentPath}/{v.Name}";
                        getPaths(__env, parentPath);
                    }
                }
            }
            
            return paths.ToArray();
        }
        public static BaseVariable[] GetAllVariables(IVariableEnvironment env)
        {
            List<BaseVariable> variables = new List<BaseVariable>();

            getVariables(env);
            void getVariables(IVariableEnvironment _env)
            {
                foreach (var v in _env)
                {
                    variables.Add(v);
                }
                foreach (var v in _env)
                {
                    if (v is IVariableEnvironment __env)
                    {
                        getVariables(__env);
                    }
                }
            }

            return variables.ToArray();
        }
        public static Type GetValueType(Type variableType)
        {
            CustomVariableAttribute att = variableType.GetCustomAttribute<CustomVariableAttribute>();
            if (att is null)
                return null;
            return att.TargetType;
        }
        public static string GetMenuPath(Type variableType)
        {
            CustomVariableAttribute att = variableType.GetCustomAttribute<CustomVariableAttribute>();
            if (att is null)
                return null;
            return att.MenuPath;
        }
        public static bool TryGetVariable(this IVariableEnvironment env, string name, out BaseVariable variable)
        {
            variable = null;
            foreach (var v in env)
            {
                if (v.GetName() == name)
                {
                    variable = v;
                    break;
                }
            }
            if (variable is null)
            {
                foreach (var v in env)
                {
                    if (v is IVariableEnvironment _env)
                    {
                        if (TryGetVariable(_env, name, out var resultVariable))
                        {
                            variable = resultVariable;
                            break;
                        }
                    }
                }
            }
            return variable is object;
        }
        public static bool TryGetValueType(this IVariableEnvironment env, string name, out Type type)
        {
            if (TryGetVariable(env, name, out BaseVariable variable))
            {
                type = GetValueType(variable.GetType());
                return true;
            }
            else
            {
                type = null;
                return false;
            }
        }
#if PLAYMAKER
        public static bool TryGetEnvironment(this PlayMakerFSM fsm, out IVariableEnvironment env)
        {
            env = null;
            if (fsm is null || fsm.Equals(null))
                return false;
            if (fsm.TryGetComponent(out FsmLiteralExecuteComponent comp))
            {
                if (comp.Pairs.TryGetValue(fsm, out VariableAsset asset))
                {
                    env = asset;
                    return true;
                }
                else
                {
                    env = asset = createAsset();
                    comp.Pairs[fsm] = asset;
                    return true;
                }
            }
            else
            {
                comp = fsm.gameObject.AddComponent<FsmLiteralExecuteComponent>();
                var asset = createAsset();
                env = asset;
                comp.Pairs[fsm] = asset;
                return true;
            }

            VariableAsset createAsset()
            {
                VariableAsset _asset = ScriptableObject.CreateInstance<VariableAsset>();
                _asset.name = $"{fsm.name}:{fsm.FsmName}";
                foreach (var _named in fsm.FsmVariables.GetAllNamedVariables())
                {
                    if (TryCastFsmVariable(fsm, _named, out BaseVariable _iVar))
                        _asset.Variables.Add(_iVar);
                }
                return _asset;
            }
        }
        public static bool TryCastFsmVariable(PlayMakerFSM fsm, NamedVariable named, out BaseVariable iVar)
        {
            iVar = null;
            if (named is null)
                return false;

            switch (named.VariableType)
            {
                default:
                    throw new NotImplementedException();
                case VariableType.Array:
                    throw new NotImplementedException();
                case VariableType.Bool:
                    iVar = new FsmBoolVariable(fsm, named);
                    return true;
                case VariableType.Color:
                    iVar = new FsmColorVariable(fsm, named);
                    return true;
                case VariableType.Enum:
                    iVar = new FsmEnumVariable(fsm, named);
                    return true;
                case VariableType.Float:
                    iVar = new FsmFloatVariable(fsm, named);
                    return true;
                case VariableType.GameObject:
                    iVar = new FsmGameObjectVariable(fsm, named);
                    return true;
                case VariableType.Int:
                    iVar = new FsmIntVariable(fsm, named);
                    return true;
                case VariableType.Material:
                    iVar = new FsmMaterialVariable(fsm, named);
                    return true;
                case VariableType.Object:
                    iVar = new FsmObjectVariable(fsm, named);
                    return true;
                case VariableType.Quaternion:
                    iVar = new FsmQuaternionVariable(fsm, named);
                    return true;
                case VariableType.Rect:
                    iVar = new FsmRectVariable(fsm, named);
                    return true;
                case VariableType.String:
                    iVar = new FsmStringVariable(fsm, named);
                    return true;
                case VariableType.Texture:
                    iVar = new FsmTextureVariable(fsm, named);
                    return true;
                case VariableType.Vector3:
                    iVar = new FsmVector3Variable(fsm, named);
                    return true;
                case VariableType.Vector2:
                    iVar = new FsmVector2Variable(fsm, named);
                    return true;
            }
        }
#endif
    }
}