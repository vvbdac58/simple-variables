﻿namespace SimpleVariables
{
    public enum OperaterName
    {
        Unexpected = 0,

        Dot,
        Comma,

        Negate,
        Not,

        Multiply,
        Division,
        Modulus,

        Addition,
        Subtraction,

        LeftShift,
        RightShift,

        GreaterThan,
        LessThan,

        GreaterThanOrEqual,
        LessThanOrEqual,

        Equality,
        Inequality,

        BitwiseAnd,
        
        BitwiseOr,

        LogicAnd,

        LogicOr,

        Assign,

        NotAssign,

        AddAssign,
        SubtractAssign,

        MultiplyAssign,
        DivideAssign,
        ModulosAssign,

        AndAssign,
        OrAssign,
    }
}