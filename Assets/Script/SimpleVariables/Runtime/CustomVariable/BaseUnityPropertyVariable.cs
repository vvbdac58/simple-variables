using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    public abstract class BaseUnityPropertyVariable : BaseVariable
    {
#if UNITY_EDITOR
        public override float GetPropertyHeight(SerializedProperty prop, float width)
        {
            if (IsExpanded)
                return
                    lineHeight +
                    spacing +
                    base.GetPropertyHeight(prop, width);
            else
                return base.GetPropertyHeight(prop, width);
        }
        public virtual void UnityPropertyField(Rect position, GUIContent label, SerializedProperty prop)
        {
            EditorGUI.PropertyField(position, prop, label);
        }
        public override void FoldedField(Rect position, GUIContent label, SerializedProperty prop, bool editable)
        {
            EditorGUI.BeginDisabledGroup(!editable);
            EditorGUI.PropertyField(position, prop, empty);
            EditorGUI.EndDisabledGroup();
        }
        public override void PropertyField(
            Rect position,
            SerializedProperty prop,
            bool editable,
            out Vector2 pos, out Vector2 size)
        {
            base.PropertyField(position, prop, editable, out pos, out size);

            EditorGUI.indentLevel++;
            if (IsExpanded)
            {
                pos.y += spacing + size.y;
                size = new Vector2(size.x, lineHeight);
                var rect = new Rect(pos, size);
                var relativeProp = prop.FindPropertyRelative(ValueFieldName);
                EditorGUI.BeginDisabledGroup(!editable);
                EditorGUI.BeginProperty(rect, new GUIContent(ValueFieldName), relativeProp);
                var nicifiedContent = new GUIContent(relativeProp.displayName);
                UnityPropertyField(rect, nicifiedContent, relativeProp);
                EditorGUI.EndProperty();
                EditorGUI.EndDisabledGroup();
            }
            EditorGUI.indentLevel--;
        }
#endif
    }
}