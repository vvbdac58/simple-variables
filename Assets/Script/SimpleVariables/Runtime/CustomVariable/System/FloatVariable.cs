﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor; 
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(float), nameof(Float), "System/Float")]
    public class FloatVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public float Float = 0f;

        public FloatVariable()
        {
            Name = "Float";
            Float = 0f;
        }

        public override object Value
        {   
            get => Float; 
            set
            {
                if (value is int i)
                    Float = i;
                else if (value is double d)
                    Float = Convert.ToSingle(d);
                else if (value is decimal m)
                    Float = Convert.ToSingle(m);
                else
                    Float = (float)value;
            }
        }
        public override string ValueFieldName
            => nameof(Float);
        public override Type ValueType
            => typeof(float);

#if UNITY_EDITOR
        public override void UnityPropertyField(Rect position, GUIContent label, SerializedProperty prop)
        {
            base.UnityPropertyField(position, label, prop);
            EditorGUI.LabelField(position,
                empty,
                label,
                CustomGUIStyles.MiniLowerRightHintLabel);
        }
        public override void FoldedField(Rect position, GUIContent label, SerializedProperty prop, bool editable)
        {
            EditorGUI.BeginDisabledGroup(!editable);
            base.FoldedField(position, label, prop, editable);
            EditorGUI.LabelField(position,
                empty,
                label,
                CustomGUIStyles.MiniLowerRightHintLabel);
            EditorGUI.EndDisabledGroup();
        }
#endif
    }
}