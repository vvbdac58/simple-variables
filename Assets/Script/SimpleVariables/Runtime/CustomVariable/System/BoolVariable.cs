﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(bool), nameof(Bool), "System/Bool")]
    public class BoolVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        [ContextMenuItem("Show Used Objects", "ShowUsedObjects")]
        public bool Bool = false;

        public BoolVariable()
        {
            Name = "Bool";
            Bool = false;
        }

        public override object Value
        {    
            get => Bool;
            set
            {
                if (value is int i)
                    Bool = i == 1;
                else if (value is byte b)
                    Bool = b == 1;
                else
                    Bool = (bool)value;
            }
        } 
        public override string ValueFieldName
            => nameof(Bool);
        public override Type ValueType
            => typeof(bool);

#if UNITY_EDITOR
#endif
    }
}