using System;
using UnityEngine;
using System.Linq;
using System.Text.RegularExpressions;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(Type), nameof(QualifiedName), "System/Type")]
    public class TypeVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public string QualifiedName = default;

        public TypeVariable()
        {
            Name = nameof(Type);
            QualifiedName = default;
        }

        public string FullName
            => string.IsNullOrEmpty(QualifiedName) ?
            string.Empty :
            QualifiedName.Split(",", StringSplitOptions.RemoveEmptyEntries)[0];
        public override object Value
        {    
            get => Type.GetType(QualifiedName);
            set 
            {
                if (value is string str)
                    QualifiedName = str;
                else if (value is Type type)
                    QualifiedName = type.AssemblyQualifiedName;
                else
                    QualifiedName = value.GetType().AssemblyQualifiedName;
            }
        } 
        public override string ValueFieldName
            => nameof(QualifiedName);
        public override Type ValueType
            => typeof(Type);

#if UNITY_EDITOR
        private Type[] CollectTypes()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .Where(assem => PreferencesSettings.Instance.UsingAssemblies.Contains(assem.GetName().Name))
                .SelectMany(assem => assem.GetTypes()).Where(type => !type.IsSpecialName)
                .OrderBy(type => type.FullName).ToArray();
        }
        private static readonly Regex ReplaceRegex = new Regex("[.+]");
        private GUIContent GetContent(Type type)
        {
            return new GUIContent(ReplaceRegex.Replace(type.FullName, "/"));
        }
        private bool ValidateQualifiedName(string name)
        {
            return Type.GetType(name) is object;
        }
        public override float GetPropertyHeight(SerializedProperty prop, float width)
        {
            if (IsExpanded)
            {
                return base.GetPropertyHeight(prop, width)
                    + lineHeight + spacing;
            }
            else
            {
                return base.GetPropertyHeight(prop, width);
            }
        }
        public override void UnityPropertyField(Rect position, GUIContent label, SerializedProperty prop)
        {
            //prop.TypeValue = EditorGUI.TypeField(position, label, prop.TypeValue);
            var pos = position.position;
            var size = new Vector2(position.width, lineHeight);
            {
                var rectPos = pos;
                var rectSize = size;
                var rect = new Rect(rectPos, rectSize);
                var guic = GUI.color;
                if (!ValidateQualifiedName(prop.stringValue))
                    GUI.color = CustomGUIStyles.RedGUIColor;
                EditorGUI.PropertyField(rect, prop);
                GUI.color = guic;
            }
            pos.y += size.y + spacing;
            {
                var rectPos = pos;
                var rectSize = new Vector2(EditorGUIUtility.labelWidth, size.y);

                rectPos.x += rectSize.x;
                rectSize = new Vector2(size.x - rectSize.x, size.y);
                var rect = new Rect(rectPos, rectSize);
                if (EditorGUI.DropdownButton(rect, new GUIContent(FullName), FocusType.Keyboard))
                {
                    GenericMenu menu = new GenericMenu();
                    foreach (var type in CollectTypes())
                    {
                        menu.AddItem(
                            GetContent(type), 
                            prop.stringValue == type.AssemblyQualifiedName,
                            () => 
                            { 
                                prop.stringValue = type.AssemblyQualifiedName;
                                prop.serializedObject.ApplyModifiedProperties();
                            });
                    }
                    menu.ShowAsContext();
                }
            }
        }
        public override void FoldedField(Rect position, GUIContent label, SerializedProperty prop, bool editable)
        {
        }
#endif
    }
}