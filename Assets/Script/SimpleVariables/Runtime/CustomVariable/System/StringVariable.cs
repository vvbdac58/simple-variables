﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor; 
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(string), nameof(String), "System/String")]
    public class StringVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public string String = string.Empty;

        public StringVariable()
        {
            Name = "String";
            String = string.Empty;
        }

        public override object Value
        {
            get => String;
            set
            {
                if (value is string str)
                    String = str;
                else
                    String = value.ToString();
            }
        }
        public override string ValueFieldName
            => nameof(String);
        public override Type ValueType
            => typeof(string);

#if UNITY_EDITOR
        public override void UnityPropertyField(Rect position, GUIContent label, SerializedProperty prop)
        {
            base.UnityPropertyField(position, label, prop);
            EditorGUI.LabelField(position,
                empty, 
                new GUIContent(ValueType.Name), 
                CustomGUIStyles.MiniLowerRightHintLabel);
        }
        public override void FoldedField(Rect position, GUIContent label, SerializedProperty prop, bool editable)
        {
            EditorGUI.BeginDisabledGroup(!editable);
            base.FoldedField(position, label, prop, editable);
            EditorGUI.LabelField(position,
                empty,
                label,
                CustomGUIStyles.MiniLowerRightHintLabel);
            EditorGUI.EndDisabledGroup();
        }
#endif
    }
}