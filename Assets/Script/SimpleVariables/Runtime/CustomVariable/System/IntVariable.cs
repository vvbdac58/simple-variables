﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(int), nameof(Int), "System/Int")]
    public class IntVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public int Int = 0;

        public IntVariable()
        {
            Name = nameof(Int);
            Int = 0;
        }

        public override object Value
        { 
            get => Int; 
            set
            {
                if (value is float f)
                    Int = Convert.ToInt32(f);
                else if (value is double d)
                    Int = Convert.ToInt32(d);
                else if (value is decimal m)
                    Int = Convert.ToInt32(m);
                else
                    Int = (int)value;
            }
        }
        public override string ValueFieldName
            => nameof(Int);
        public override Type ValueType
            => typeof(int);

#if UNITY_EDITOR
        public override void UnityPropertyField(Rect position, GUIContent label, SerializedProperty prop)
        {
            base.UnityPropertyField(position, label, prop);
            EditorGUI.LabelField(position,
                empty,
                new GUIContent(ValueType.Name),
                CustomGUIStyles.MiniLowerRightHintLabel);
        }
        public override void FoldedField(Rect position, GUIContent label, SerializedProperty prop, bool editable)
        {
            EditorGUI.BeginDisabledGroup(!editable);
            base.FoldedField(position, label, prop, editable);
            EditorGUI.LabelField(position,
                empty,
                label,
                CustomGUIStyles.MiniLowerRightHintLabel);
            EditorGUI.EndDisabledGroup();
        }
#endif 

    }
}