using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SimpleVariables
{
    [CustomVariable(typeof(VariableAsset), nameof(Asset), CustomVariableContainerEnum.Asset)]
    public class VariableAssetVariable : BaseVariable, IVariableEnvironment
    {
        [SerializeField]
        public VariableAsset Asset;

        public VariableAssetVariable()
        {
            Asset = null;
            Name = nameof(Asset);
        }

        public override object Value
        { 
            get => Asset;
            set => Asset = (VariableAsset)value;            
        }
        public override string ValueFieldName 
            => nameof(Asset);
        public override Type ValueType 
            => typeof(VariableAsset);
        public override bool NameIsPersistent
        {
            get
            {
                if (Asset is null || Asset.Equals(null))
                    return false;
                if (Asset.MainAsset is null || Asset.MainAsset.Equals(null))
                    return false;
                return ReferenceEquals(Asset.MainAsset, Asset);
            }
        }

        #region IVariableContainer
        public IEnumerator<BaseVariable> GetEnumerator()
        {
            return ((IEnumerable<BaseVariable>)Asset).GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)Asset).GetEnumerator();
        }
        #endregion

#if UNITY_EDITOR
        public override float GetPropertyHeight(SerializedProperty prop, float width)
        {
            if (IsExpanded)
            {
                if (Asset is object && !Asset.Equals(null))
                {
                    return
                       spacing * 2f +
                       lineHeight +
                       base.GetPropertyHeight(prop, width) +
                       Asset.GetReorderableListHeight(width - 5f);
                }
                else
                {
                    return
                       spacing * 2f +
                       lineHeight +
                       base.GetPropertyHeight(prop, width);
                }
            }
            else
            {
                return base.GetPropertyHeight(prop, width);
            }
        }
        public override void PropertyField(
            Rect position, 
            SerializedProperty prop, 
            bool editable, 
            out Vector2 pos, out Vector2 size)
        {
            base.PropertyField(position, prop, editable, out pos, out size);

            EditorGUI.indentLevel++;
            if (IsExpanded)
            {
                pos.y += spacing + size.y;
                var relativeProp = prop.FindPropertyRelative(nameof(Asset));
                EditorGUI.BeginDisabledGroup(!editable);
                {
                    size = new Vector2(size.x, lineHeight);

                    var fieldPos = new Vector2(pos.x, pos.y);
                    var fieldSize = new Vector2(size.x - 50f - spacing, size.y);
                    EditorGUI.ObjectField(new Rect(fieldPos, fieldSize), relativeProp, new GUIContent(string.Empty));
                    if (Asset is object && !Asset.Equals(null))
                    {
                        if (!NameIsPersistent)
                        {
                            Asset.name = Name;
                            relativeProp.serializedObject.ApplyModifiedProperties();
                        }
                        else
                        {
                            Name = Asset.name;
                            prop.serializedObject.ApplyModifiedProperties();
                        }
                    }

                    if (prop.serializedObject.targetObject is VariableAsset upperAsset &&
                        upperAsset.MainAsset is object && !upperAsset.MainAsset.Equals(null))
                    {
                        bool needed = relativeProp.objectReferenceValue is null;
                        EditorGUI.BeginDisabledGroup(!needed);
                        string tooltip = needed ? "Create SubAsset" : "Please clear reference first.";
                        var newPos = new Vector2(fieldPos.x + fieldSize.x + spacing, pos.y);
                        var newSize = new Vector2(50f, size.y);
                        if (GUI.Button(new Rect(newPos, newSize), new GUIContent("Create", tooltip)))
                        {
                            relativeProp.objectReferenceValue = VariableAsset.CreateSubAsset(upperAsset.MainAsset);
                            relativeProp.serializedObject.ApplyModifiedProperties();
                        }
                        EditorGUI.EndDisabledGroup();
                    }

                    if (relativeProp.objectReferenceValue is VariableAsset asset
                        && asset is object && !asset.Equals(null))
                    {
                        pos.y += spacing + size.y;
                        pos = new Vector2(pos.x, pos.y);
                        size.x -= 10f;
                        size = new Vector2(size.x, asset.GetReorderableListHeight(size.x));

                        pos.x += 15f;
                        size.x -= 5f;

                        /// Gets the cached editor for re-drawing the ReorderableList,
                        /// it makes the list can be click inside another.
                        var rect = new Rect(pos, size);
                        var editor = asset.CachedEditor;
                        EditorGUI.BeginProperty(rect, new GUIContent(nameof(Asset)), relativeProp);
                        editor.OnInspectorGUI(rect);
                        EditorGUI.EndProperty();
                    }
                }
                EditorGUI.EndDisabledGroup();
            }
            EditorGUI.indentLevel--;
        }
#endif
    }
}