using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    /// <summary>
    /// This is the base class for all variables.
    /// <br> Hovever, the editor itself may be buggy when using default property field,
    /// so we implemented it in class functions. </br>
    /// </summary>
    [Serializable]
    public class BaseVariable : IName
    {
        /// <summary>
        /// For user to note the name of the variable.
        /// <br> It can be multiple same name in the list. </br>
        /// </summary>
        public string Name = "Base";
        /// <summary>
        /// For user to note the meaning of the variable.
        /// </summary>
        [TextArea(1, 10)]
        public string Hint = "Hint";

        /// <summary>
        /// Generically provides the value.
        /// <br> Child class should always override it,
        /// for system may use this property! </br>
        /// </summary>
        public virtual object Value
        {   
            get => throw new NotImplementedException(); 
            set => throw new NotImplementedException();
        }
        /// <summary>
        /// Returns the name of field, storing the value, that implemented this class.
        /// </summary>
        public virtual string ValueFieldName
            => throw new NotImplementedException();
        /// <summary>
        /// Provides type of the value.
        /// <br> Child class should always override it,
        /// for system may use this property! </br>
        /// </summary>
        public virtual Type ValueType
            => null;
        /// <summary>
        /// Provides the name of this variable is persistent or not.
        /// <br> Child class should always override it,
        /// for system may use this property! </br>
        /// <br> Check out VariableAssetVariable for details of overrides. </br>
        /// </summary>
        public virtual bool NameIsPersistent
            => false;

        /// <summary>
        /// Counts literal that uses this variable.
        /// </summary>
        [NonSerialized]
        public List<UnityEngine.Object> UsingObjects;
        /// <summary>
        /// UsedObjects is object and UsedObjects.Count > 0
        /// </summary>
        public bool HasUsers
            => UsingObjects is object && UsingObjects.Count > 0;
        /// <summary>
        /// Tells the variable is being used in this frame.
        /// <br> We only uses this in editor, because it is consumes a lot in update! </br>
        /// </summary>
        [NonSerialized]
        public int UsedFrame;

        public void AddUsingObject(UnityEngine.Object obj)
        {
            if (UsingObjects is null)
                UsingObjects = new List<UnityEngine.Object>();
            UsingObjects.Add(obj);
        }
        public void RemoveUsingObject(UnityEngine.Object obj)
        {
            if (UsingObjects is null)
                return;
            UsingObjects.Remove(obj);
        }

        public string GetName()
        {
            return Name;
        }

#if UNITY_EDITOR
        /// <summary>
        /// Control the expansion of drawing in editor
        /// </summary>
        [HideInInspector]
        public bool IsExpanded = false;
        /// <summary>
        /// new GUIContent(string.Empty)
        /// </summary>
        public static GUIContent empty = new GUIContent(string.Empty);
        /// <summary>
        /// EditorGUIUtility.singleLineHeight
        /// </summary>
        protected float lineHeight
            => EditorGUIUtility.singleLineHeight;
        /// <summary>
        /// EditorGUIUtility.standardVerticalSpacing
        /// </summary>
        protected float spacing
            => EditorGUIUtility.standardVerticalSpacing;
        /// <summary>
        /// EditorGUIUtility.labelWidth
        /// </summary>
        protected float labelWidth
            => EditorGUIUtility.labelWidth;
        /// <summary>
        /// EditorStyles.boldLabel
        /// </summary>
        protected static GUIStyle boldLabelStyle
            => EditorStyles.boldLabel;

        /// <summary>
        /// Get the calculated height for layout for BaseVariable.Hint only.
        /// </summary>
        /// <param name="width"> Used to pass the designated width. </param>
        /// <returns></returns>
        public virtual float GetHintHeight(SerializedProperty prop, float width)
        {
            return EditorStyles.textArea.CalcHeight(new GUIContent(Hint), width);
        }
        /// <summary>
        /// Get the calculated height for layout.
        /// </summary>
        /// <param name="width"> Used to pass the designated width. </param>
        /// <returns></returns>
        public virtual float GetPropertyHeight(SerializedProperty prop, float width)
        {
            if (IsExpanded)
                return
                    lineHeight +
                    spacing +
                    GetHintHeight(prop, width);
            else
                return
                    spacing +
                    lineHeight;
        }
        /// <summary>
        /// Redesigning property field.
        /// We make sure that data is transferred inside SerializedProperty,
        /// that we can pass apply-dirty-value's-change to Unity.
        /// <br> Returning pos and size was designed for child classes to get where last
        /// property is drawn. In BaseVariable's case it is BaseVariable.Hint. </br>
        /// </summary>
        /// <param name="position"> Used to pass the control rect. </param>
        /// <param name="prop"> BaseVariable's SerializedProperty when drawing. </param>
        /// <param name="editable"> Should the field can be edit? </param>
        /// <param name="pos"> Returning where last property is drawn. </param>
        /// <param name="size"> Returning the last property size. </param>
        public virtual void PropertyField(
            Rect position,
            SerializedProperty prop,
            bool editable,
            out Vector2 pos, out Vector2 size)
        {
            var guic = GUI.color;
            if (UsedFrame == Time.frameCount)
                GUI.color = CustomGUIStyles.GreenGUIColor;
            else if (HasUsers)
                GUI.color = CustomGUIStyles.YellowGUIColor;

            ///Naming Area
            EditorGUI.indentLevel++;
            {
                pos = position.position;
                pos.y += spacing;
                size = new Vector2(position.width, lineHeight);

                {
                    var rect = new Rect(
                        pos,
                        new Vector2(10f, size.y));
                    IsExpanded = EditorGUI.Foldout(rect, IsExpanded, empty);
                }

                pos.x += 2f;
                {
                    var rect = new Rect(
                        new Vector2(pos.x, pos.y),
                        new Vector2(EditorGUIUtility.labelWidth, size.y));
                    var relativeProp = prop.FindPropertyRelative(nameof(Name));
                    /// Be aware that persistent asset cannot change file name here,
                    /// so it may not change name in eiditor to cause the error!
                    var able = IsExpanded && editable && !NameIsPersistent;
                    if (able)
                    {
                        EditorGUI.BeginProperty(rect, empty, relativeProp);
                        if (HasUsers)
                        {
                            relativeProp.stringValue = EditorGUI.TextField(rect, relativeProp.stringValue, CustomGUIStyles.GreenTextField);
                        }
                        else
                        {
                            relativeProp.stringValue = EditorGUI.TextField(rect, relativeProp.stringValue);
                        }
                        EditorGUI.EndProperty();
                    }
                    else
                    {
                        var labelRect = 
                            new Rect(
                                new Vector2(pos.x + 3f, pos.y), 
                                new Vector2(labelWidth, size.y));
                        var labelContent = new GUIContent(relativeProp.stringValue, Hint);
                        if (HasUsers)
                        {
                            EditorGUI.LabelField(labelRect, labelContent, CustomGUIStyles.GreenBoldLabel);
                            if (labelRect.Contains(Event.current.mousePosition) && Event.current.button == 1)
                            {
                                ShowUsedObjects();
                            }
                        }
                        else
                        {
                            EditorGUI.LabelField(labelRect, labelContent, CustomGUIStyles.BoldLabel);
                        }

                        GUI.color = guic;

                        var fieldRect = new Rect(
                                new Vector2(labelRect.x + labelRect.width, pos.y),
                                new Vector2(size.x - labelWidth - 3, size.y));
                        var valueProp = prop.FindPropertyRelative(ValueFieldName);
                        if (valueProp is object)
                        {
                            var valueContent = new GUIContent(valueProp.displayName);
                            if (valueProp is object)
                                FoldedField(fieldRect, valueContent, valueProp, editable);
                        }
                    }
                }
            }

            GUI.color = guic;

            ///Hint Area
            if (IsExpanded)
            {
                var hintProp = prop.FindPropertyRelative(nameof(Hint));
                pos.y += spacing + size.y;
                size = CustomEditorGUIUtility.TextAreaGetSize(hintProp, position.width);
                var rect = new Rect(pos, size);
                EditorGUI.BeginDisabledGroup(!editable);
                CustomEditorGUIUtility.TextArea(rect, hintProp);
                EditorGUI.EndDisabledGroup();
            }
            EditorGUIUtility.labelWidth = labelWidth;
            EditorGUI.indentLevel--;

        }
        /// <summary>
        /// This draws when the variable is folded.
        /// <br> Some of the variable can draw when folded in a line, but some can't. </br>
        /// </summary>
        /// <param name="position"></param>
        /// <param name="prop"></param>
        /// <param name="editable"></param>
        public virtual void FoldedField(
            Rect position,
            GUIContent label,
            SerializedProperty prop,
            bool editable)
        {
        }
        /// <summary>
        /// Shows the used object of this variable.
        /// </summary>
        public void ShowUsedObjects()
        {
            GenericMenu menu = new GenericMenu();
            foreach (var used in UsingObjects)
                menu.AddItem(new GUIContent(used.name),
                    false,
                    () => EditorGUIUtility.PingObject(used));
            menu.ShowAsContext();
        }
#endif
    }
}