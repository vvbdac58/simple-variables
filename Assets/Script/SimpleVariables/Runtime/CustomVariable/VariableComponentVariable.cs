using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor; 
#endif
using Object = UnityEngine.Object;

namespace SimpleVariables
{
    [CustomVariable(typeof(VariableComponent), "Component", CustomVariableContainerEnum.Component)]
    public class VariableComponentVariable : BaseVariable, IVariableEnvironment
    {
        [SerializeField]
        public VariableComponent Component;

        public VariableComponentVariable()
        {
            Component = null;
            Name = "Component";
        }

        public override object Value
        {   
            get => Component; 
            set => Component = (VariableComponent)value;
        }
        public override string ValueFieldName 
            => nameof(Component);
        public override Type ValueType
            => typeof(VariableComponent);
        public override bool NameIsPersistent
            => Component is object && !Component.Equals(null);

        #region IVariableContainer
        public IEnumerator<BaseVariable> GetEnumerator()
        {
            return ((IEnumerable<BaseVariable>)Component).GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)Component).GetEnumerator();
        }
        #endregion

#if UNITY_EDITOR
        public override float GetPropertyHeight(SerializedProperty prop, float width)
        {
            if (IsExpanded)
            {
                if (Component is object && !Component.Equals(null))
                {
                    return
                       spacing * 2f +
                       lineHeight +
                       base.GetPropertyHeight(prop, width) +
                       Component.GetReorderableListHeight(width - 5f);
                }
                else
                {
                    return
                       spacing * 2f +
                       lineHeight +
                       base.GetPropertyHeight(prop, width);
                }
            }
            else
            {
                return base.GetPropertyHeight(prop, width);
            }
        }
        public override void PropertyField(
            Rect position,
            SerializedProperty prop,
            bool editable,
            out Vector2 pos, out Vector2 size)
        {
            base.PropertyField(position, prop, editable, out pos, out size);

            EditorGUI.indentLevel++;
            if (IsExpanded)
            {
                pos.y += spacing + size.y;
                var relativeProp = prop.FindPropertyRelative(nameof(Component));
                EditorGUI.BeginDisabledGroup(!editable);
                {
                    size = new Vector2(size.x, lineHeight);

                    var fieldPos = new Vector2(pos.x, pos.y);
                    var fieldSize = new Vector2(size.x - 50f - spacing, size.y);
                    EditorGUI.ObjectField(new Rect(fieldPos, fieldSize), relativeProp, new GUIContent(string.Empty));
                    if (Component is object && !Component.Equals(null))
                        Name = Component.name;

                    if (prop.serializedObject.targetObject is VariableComponent upperComp)
                    {
                        bool needed = relativeProp.objectReferenceValue is null;
                        EditorGUI.BeginDisabledGroup(!needed);
                        string tooltip = needed ? "Create New Component" : "Please clear the reference first.";
                        var newPos = new Vector2(fieldPos.x + fieldSize.x + spacing, pos.y);
                        var newSize = new Vector2(50f, size.y);
                        if (GUI.Button(new Rect(newPos, newSize), new GUIContent("Create", tooltip)))
                        {
                            relativeProp.objectReferenceValue = (Object)upperComp.CreateSubComponent(upperComp.transform, Name);
                            relativeProp.serializedObject.ApplyModifiedProperties();
                        }
                        EditorGUI.EndDisabledGroup();
                    }

                    if (relativeProp.objectReferenceValue is VariableComponent comp
                        && comp is object && !comp.Equals(null))
                    {
                        pos.y += spacing + size.y;
                        pos = new Vector2(pos.x, pos.y);
                        size.x -= 10f;
                        size = new Vector2(size.x, comp.GetReorderableListHeight(size.x));

                        pos.x += 15f;
                        size.x -= 5f;

                        /// Gets the cached editor for re-drawing the ReorderableList,
                        /// it makes the list can be click inside another.
                        var rect = new Rect(pos, size);
                        var editor = comp.CachedEditor;
                        EditorGUI.BeginProperty(rect, new GUIContent(nameof(Component)), relativeProp);
                        editor.OnInspectorGUI(rect);
                        EditorGUI.EndProperty();
                    }
                }
                EditorGUI.EndDisabledGroup();
            }
            EditorGUI.indentLevel--;
        }
#endif
    }
}