using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleVariables
{
    [Serializable]
    public class Hello
    {
        public void Say()
        {
            Debug.Log("Say Hello!");
        }
        public void Say(string txt)
        {
            Debug.Log($"Say {txt}!");
        }
    }
}