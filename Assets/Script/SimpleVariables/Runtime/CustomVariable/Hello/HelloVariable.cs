using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SimpleVariables
{
    [CustomVariable(typeof(Hello), nameof(Hello))]
    public class HelloVariable : BaseVariable
    {
        public Hello Hello;

        public HelloVariable()
        {
            Name = nameof(Hello);
            Hello = new();
        }

        public override Type ValueType
            => typeof(Hello);
        public override object Value
            => Hello;
        public override string ValueFieldName 
            => nameof(Hello);
#if UNITY_EDITOR
        public override float GetPropertyHeight(SerializedProperty prop, float width)
        {
            var height = base.GetPropertyHeight(prop, width);
            if (IsExpanded)
                height += lineHeight + spacing;
            return height;
        }
        public override void PropertyField(Rect position, SerializedProperty prop, bool editable, out Vector2 pos, out Vector2 size)
        {
            base.PropertyField(position, prop, editable, out pos, out size);
            if (IsExpanded)
            {
                EditorGUI.indentLevel++;
                pos.y += size.y + spacing;
                size = new Vector2(size.x, lineHeight);
                var rect = new Rect(pos, size);
                EditorGUI.LabelField(rect, "Say Hello!");
                EditorGUI.indentLevel--;
            }
        }
#endif
    }
}