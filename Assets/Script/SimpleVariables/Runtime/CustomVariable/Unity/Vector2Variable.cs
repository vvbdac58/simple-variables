using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(Vector2), nameof(Vector2), "Unity/Vector2")]
    public class Vector2Variable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public Vector2 Vector2 = default;

        public Vector2Variable()
        {
            Name = nameof(Vector2);
            Vector2 = default;
        }

        public override object Value
        {
            get => Vector2; 
            set => Vector2 = (Vector2)value;
        }
        public override string ValueFieldName
            => nameof(Vector2);
        public override Type ValueType
            => typeof(Vector2);

#if UNITY_EDITOR
#endif
    }
}