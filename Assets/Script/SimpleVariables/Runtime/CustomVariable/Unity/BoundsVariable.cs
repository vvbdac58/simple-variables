using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(Bounds), nameof(Bounds), "Unity/Bounds")]
    public class BoundsVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public Bounds Bounds = default;

        public BoundsVariable()
        {
            Name = nameof(Bounds);
            Bounds = default;
        }

        public override object Value
        {   
            get => Bounds;
            set => Bounds = (Bounds)value;
        }
        public override string ValueFieldName
            => nameof(Bounds);
        public override Type ValueType
            => typeof(Bounds);

#if UNITY_EDITOR
        public override float GetPropertyHeight(SerializedProperty prop, float width)
        {
            if (!IsExpanded)
                return base.GetHintHeight(prop, width);
            else
                return base.GetHintHeight(prop, width)
                    + (lineHeight + spacing) * 4f;
        }
        public override void FoldedField(Rect position, GUIContent label, SerializedProperty prop, bool editable)
        {
        }
#endif

    }
}