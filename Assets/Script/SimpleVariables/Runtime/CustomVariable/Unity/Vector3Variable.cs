using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(Vector3), nameof(Vector3), "Unity/Vector3")]
    public class Vector3VariableVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public Vector3 Vector3 = default;

        public Vector3VariableVariable()
        {
            Name = nameof(Vector3);
            Vector3 = default;
        }

        public override object Value
        {
            get => Vector3; 
            set => Vector3 = (Vector3)value;
        }
        public override string ValueFieldName
            => nameof(Vector3);
        public override Type ValueType
            => typeof(Vector3);

#if UNITY_EDITOR
#endif
    }
}