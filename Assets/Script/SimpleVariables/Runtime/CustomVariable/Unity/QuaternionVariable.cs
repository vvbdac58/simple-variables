using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(Quaternion), nameof(Rotation), "Unity/Rotation")]
    public class QuaternionVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public Quaternion Rotation = default;

        public QuaternionVariable()
        {
            Name = nameof(Rotation);
            Rotation = default;
        }

        public override object Value
        {
            get => Rotation; 
            set => Rotation = (Quaternion)value;
        }
        public override string ValueFieldName
            => nameof(Rotation);
        public override Type ValueType
            => typeof(Quaternion);

#if UNITY_EDITOR
#endif
    }
}