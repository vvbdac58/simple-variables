using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(Component), nameof(Component), "Unity/Component")]
    public class ComponentVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public Component Component = default;

        public ComponentVariable()
        {
            Name = nameof(Component);
            Component = default;
        }

        public override object Value
        {
            get => Component; 
            set => Component = (Component)value;
        }
        public override string ValueFieldName
            => nameof(Component);
        public override Type ValueType
            => typeof(Component);

#if UNITY_EDITOR
#endif
    }
}