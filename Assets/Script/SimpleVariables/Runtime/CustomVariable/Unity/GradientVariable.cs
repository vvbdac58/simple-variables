using System;
using System.Reflection;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(Gradient), nameof(Gradient), "Unity/Gradient")]
    public class GradientVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public Gradient Gradient = default;

        public GradientVariable()
        {
            Name = nameof(Gradient);
            Gradient = default;
        }

        public override object Value
        {
            get => Gradient; 
            set => Gradient = (Gradient)value;
        }
        public override string ValueFieldName
            => nameof(Gradient);
        public override Type ValueType
            => typeof(Gradient);

#if UNITY_EDITOR
#endif
    }
}