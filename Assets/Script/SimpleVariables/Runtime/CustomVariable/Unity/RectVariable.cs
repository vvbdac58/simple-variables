using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(Rect), nameof(Rect), "Unity/Rect")]
    public class RectVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public Rect Rect = default;

        public RectVariable()
        {
            Name = nameof(Rect);
            Rect = default;
        }

        public override object Value
        {
            get => Rect; 
            set => Rect = (Rect)value;
        }
        public override string ValueFieldName
            => nameof(Rect);
        public override Type ValueType
            => typeof(Rect);

#if UNITY_EDITOR
        public override float GetPropertyHeight(SerializedProperty prop, float width)
        {
            if (!IsExpanded)
                return base.GetPropertyHeight(prop, width);
            else
                return base.GetPropertyHeight(prop, width) + lineHeight + spacing;
        }
        public override void FoldedField(Rect position, GUIContent label, SerializedProperty prop, bool editable)
        {
        }
#endif
    }
}