using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleVariables
{
    [Serializable]
    [CustomVariable(typeof(Color), nameof(Color), "Unity/Color")]
    public class ColorVariable : BaseUnityPropertyVariable
    {
        [SerializeField]
        public Color Color = default;

        public ColorVariable()
        {
            Name = nameof(Color);
            Color = default;
        }

        public override object Value
        { 
            get => Color;
            set => Color = (Color)value;
        }
        public override string ValueFieldName
            => nameof(Color);
        public override Type ValueType
            => typeof(Color);

#if UNITY_EDITOR
#endif

    }
}