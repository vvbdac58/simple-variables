using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor; 
#endif

namespace SimpleVariables
{
    public class TestsComponent : MonoBehaviour
    {
        public string Literal;
        public VariableComponent Environment;
        [ContextMenu("Test It")]
        public void TestIt()
        {
            ExpressionUtility.Compile<Action>(Literal, out _, Environment).Invoke();
        }
        private void Update()
        {
            TestIt();
        }
    }
}