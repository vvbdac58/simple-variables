using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Debug = UnityEngine.Debug;
#if UNITY_EDITOR
using UnityEditor; 
#endif

namespace SimpleVariables
{
    public class Tests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestsSimplePasses()
        {
            VariableAsset asset = AssetDatabase.LoadAssetAtPath<VariableAsset>("Assets/Script/SimpleVariables/Runtime/Tests/Test Asset.asset");
            if (asset is null || asset.Equals(null))
            {
                LogAssert.Expect(LogType.Error, $"Asset is null!");
                return;
            }

            try
            {
                //ExpressionUtility.Process("Bool = !Bool", asset);
            }
            catch (Exception ex)
            {
                LogAssert.Expect(LogType.Exception, ex.Message);
            }
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator TestsWithEnumeratorPasses()
        {
            int frame = 1999;

            VariableAsset asset = AssetDatabase.LoadAssetAtPath<VariableAsset>("Assets/Script/SimpleVariables/Runtime/Tests/Test Asset.asset");
            if (asset is null || asset.Equals(null))
            {
                LogAssert.Expect(LogType.Error, $"Asset is null!");
                yield break;
            }

            try
            {
                //ExpressionUtility.Process("Bool = !Bool", asset);
            }
            catch (Exception ex)
            {
                LogAssert.Expect(LogType.Exception, ex.Message);
                yield break;
            }
            
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            if (frame < 0)
            {
                LogAssert.Expect(LogType.Log, $"Finished!");
                yield break;
            }
            else
            {
                yield return null;
            }
        }
    }
}