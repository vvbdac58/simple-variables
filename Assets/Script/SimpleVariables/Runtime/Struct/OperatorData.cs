﻿using System.Text.RegularExpressions;

namespace SimpleVariables
{
    public struct OperatorData
    {
        /// <summary>
        /// tells the operator is an exception
        /// </summary>
        public bool unexpected;
        /// <summary>
        /// operator is unary or not
        /// </summary>
        public bool unary;
        /// <summary>
        /// operator is binary or not
        /// </summary>
        public bool binary;
        /// <summary>
        /// operator's priority when parsing
        /// </summary>
        public int prior;
        /// <summary>
        /// text represents the operator
        /// </summary>
        public string text;
        /// <summary>
        /// regex used to seperator operator from normal literal
        /// </summary>
        public Regex regex;
        /// <summary>
        /// the name used to switch what type is it
        /// </summary>
        public OperaterName name;

        /// <summary>
        /// We implemented this one when encountering unexpected
        /// the operator we didn't deal with.
        /// </summary>
        /// <param name="unexcepted"> tells the operator is an exception </param>
        public OperatorData(bool unexcepted)
        {
            this.unexpected = unexcepted;

            this.unary = default;
            this.binary = default;
            this.prior = default;
            this.text = default;
            this.regex = default;
            this.name = default;
        }
        /// <summary>
        /// We implemented the ctor just to make coding easier for single-liner.
        /// </summary>
        /// <param name="unary"> operator is unary or not </param>
        /// <param name="binary"> operator is binary or not </param>
        /// <param name="prior"> operator's priority when parsing </param>
        /// <param name="text"> text represents the operator </param>
        /// <param name="regex"> regex used to seperator operator from normal literal </param>
        /// <param name="name"> the name used to switch what type is it </param>
        public OperatorData(bool unary, bool binary, int prior, string text, Regex regex, OperaterName name)
            : this(false)
        {
            this.unary = unary;
            this.binary = binary;
            this.prior = prior;
            this.text = text;
            this.regex = regex;
            this.name = name;
        }
    }

    public struct LexemeData
    {
        public bool unexpected;
        public bool isVariable;
        public bool isFieldOrProperty;

        public LexemeData(bool unexpected)
        {
            this.unexpected = unexpected;

            this.isVariable = default;
            this.isFieldOrProperty = default;
        }

        public LexemeData(bool isHead, bool isMethod)
            : this(false)
        {
            this.isVariable = isHead;
            this.isFieldOrProperty = !isHead && !isMethod;
        }
    }
}