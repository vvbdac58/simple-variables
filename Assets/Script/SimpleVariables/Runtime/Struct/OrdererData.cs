﻿namespace SimpleVariables
{
    public struct OrdererData
    {
        public OrdererData(char character)
        {
            this.character = character;

            if (character == '(')
            {
                pairedCharacter = ')';

                isInvalid = false;

                isOpenning = true;
                isClosing = false;

                isParameters = true;
                isIndexer = false;
                isScope = false;
            }
            else if (character == ')')
            {
                pairedCharacter = '(';

                isInvalid = false;

                isOpenning = false;
                isClosing = true;

                isParameters = true;
                isIndexer = false;
                isScope = false;
            }
            else if (character == '[')
            {
                pairedCharacter = ']';

                isInvalid = false;

                isOpenning = true;
                isClosing = false;

                isParameters = false;
                isIndexer = true;
                isScope = false;
            }
            else if (character == ']')
            {
                pairedCharacter = '[';
                isInvalid = false;

                isOpenning = false;
                isClosing = true;

                isParameters = false;
                isIndexer = true;
                isScope = false;
            }
            else if (character == '{')
            {
                pairedCharacter = '}';
                isInvalid = false;

                isOpenning = true;
                isClosing = false;

                isParameters = false;
                isIndexer = false;
                isScope = true;
            }
            else if (character == '}')
            {
                pairedCharacter = '{';
                isInvalid = false;
                isOpenning = false;
                isClosing = true;

                isParameters = false;
                isIndexer = false;
                isScope = true;
            }
            else
            {
                pairedCharacter = '\0';
                isInvalid = true;

                isOpenning = false;
                isClosing = false;

                isParameters = false;
                isIndexer = false;
                isScope = false;
            }
        }

        public bool IsPair(OrdererData paired)
        {
            return !paired.isInvalid && 
                pairedCharacter == paired.character;
        }

        public char character;
        public char pairedCharacter;
        public bool isInvalid;
        public bool isOpenning;
        public bool isClosing;
        public bool isParameters;
        public bool isIndexer;
        public bool isScope;
    }
}