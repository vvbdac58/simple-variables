using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace SimpleVariables
{
    /// <summary>
    /// This is a delecate class for loop serialization with variables.
    /// <br> It stores variables to be used. Also, BaseVariable can store VariableAsset,
    /// too, at the same time. </br>
    /// </summary>
    [CreateAssetMenu(menuName = "Simple Variables/Variable Asset", order = 100)]
    public partial class VariableAsset : ScriptableObject, IVariableEnvironment
    {
        /// <summary>
        /// Stores the main asset for data saving in the Project.
        /// <br> We need to add Sub Asset </br>
        /// </summary>
        [SerializeField]
        public VariableAsset MainAsset;
        /// <summary>
        /// Saves all the custom variables.
        /// </summary>
        [SerializeReference]
        public List<BaseVariable> Variables = new List<BaseVariable>();

        #region IVariableContainer
        public string GetName()
        {
            return name;
        }
        public IEnumerator<BaseVariable> GetEnumerator()
        {
            return Variables.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return Variables.GetEnumerator();
        }
        #endregion

#if UNITY_EDITOR
        public bool IsMainAsset
            => AssetDatabase.IsMainAsset(this);

        public static VariableAsset CreateSubAsset(VariableAsset mainAsset)
        {
            var instace = CreateInstance<VariableAsset>();
            AssetDatabase.AddObjectToAsset(instace, mainAsset);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(mainAsset));
            instace.MainAsset = mainAsset;
            return instace;
        }

        public void OnValidate()
        {
            if (IsMainAsset)
                MainAsset = this;
        }
#endif

#if UNITY_EDITOR
        public const float REORDERABLE_HEIGHT = 37f;

        /// <summary>
        /// We made the ReorderableList can be drawn inside another ReorderableList,
        /// by creating and caching the editor inside class.
        /// </summary>
        private VariableAssetEditor cachedEditor;
        private ReorderableList cachedList;

        private void CreateEditor()
        {
            cachedEditor = (VariableAssetEditor)Editor.CreateEditor(this);
            CreateList();
        }
        public VariableAssetEditor CachedEditor
        {
            get
            {
                if (cachedEditor is null)
                    CreateEditor();
                return cachedEditor;
            }
            set
            {
                cachedEditor = value;
            }
        }
        public SerializedObject CachedObject
        {
            get
            {
                try
                {
                    var prop = CachedEditor.serializedObject.FindProperty(nameof(Variables));
                    return CachedEditor.serializedObject;
                }
                catch
                {
                    CreateEditor();
                    return CachedEditor.serializedObject;
                }
            }
        }
        private void CreateList()
        {
            try
            {
                _createList();
            }
            catch
            {
                CreateEditor();
                _createList();
            }

            void _createList()
            {
                var obj = CachedObject;
                var prop = CachedObject.FindProperty(nameof(Variables));
                cachedList = new ReorderableList(obj, prop, true, false, true, true)
                {
                    onAddDropdownCallback = _onAddDropDown,
                    elementHeightCallback = _elementHeight,
                    drawElementCallback = _drawElement,
                    onChangedCallback = _onChanged,
                };
            }
            void _onAddDropDown(Rect buttonRect, ReorderableList list)
            {
                VariableUtility.AllCustomVariableMenu(list, CachedObject, Variables);
            }
            float _elementHeight(int _index)
            {
                var _prop = CachedList.serializedProperty.GetArrayElementAtIndex(_index);
                if (_prop.managedReferenceValue is BaseVariable baseV)
                    return baseV.GetPropertyHeight(_prop, EditorGUIUtility.currentViewWidth);
                else
                    throw new InvalidOperationException();
            }
            void _drawElement(Rect _rect, int _index, bool _isActive, bool _isFocused)
            {
                var _prop = CachedList.serializedProperty.GetArrayElementAtIndex(_index);
                if (_prop.managedReferenceValue is BaseVariable baseV)
                    baseV.PropertyField(_rect, _prop, _isActive, out _, out _);
                else
                    throw new InvalidOperationException();
            }
            void _onChanged(ReorderableList list)
            {

            }
        }
        public ReorderableList CachedList
        {
            get
            {
                try
                {
                    if (cachedList is null)
                        CreateList();
                    return cachedList;
                }
                catch
                {
                    CreateList();
                    return cachedList;
                }
            }
        }

        public float GetReorderableListHeight(float width)
        {
            var height = REORDERABLE_HEIGHT;

            AddToHeight(CachedObject);
            void AddToHeight(SerializedObject _obj)
            {
                if (!(_obj.targetObject is VariableAsset))
                    return;

                var _variables = _obj.FindProperty(nameof(Variables));
                if (_variables.arraySize > 0)
                {
                    for (int _i = 0; _i < _variables.arraySize; _i++)
                    {
                        var _inner = _variables.GetArrayElementAtIndex(_i);
                        if (_inner.managedReferenceValue is BaseVariable _eBaseVar)
                            height += _eBaseVar.GetPropertyHeight(_inner, width);
                    }
                }
                else
                {
                    height += EditorGUIUtility.singleLineHeight;
                }
            }
         
            return height;
        }
#endif
    }
}