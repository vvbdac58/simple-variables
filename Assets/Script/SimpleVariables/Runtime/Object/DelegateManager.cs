using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif

namespace SimpleVariables
{
    public static class DelegateManager
    {
        public static Dictionary<DelegateKey, DelegateData> Pairs = new Dictionary<DelegateKey, DelegateData>();

        public static void Purge()
        {
            List<DelegateKey> purgers = new List<DelegateKey>();
            foreach (var pair in Pairs)
            {
                bool owned = false;
                foreach (var owner in pair.Value.Owners)
                {
                    owned |= owner.WithObject is object && !owner.WithObject.Equals(null);
                    if (owned)
                    {
                        break;
                    }
                }
                if (owned)
                {
                    continue;
                }
                else
                {
                    pair.Value.Dispose();
                    purgers.Add(pair.Key);
                }
            } 
            foreach (var purger in purgers)
            {
                Pairs.Remove(purger);
            }
        }
    } 
}