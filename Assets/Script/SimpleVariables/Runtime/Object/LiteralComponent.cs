using UnityEngine;

namespace SimpleVariables
{
    [AddComponentMenu("Simple Variables/Literal Component")]
    public class LiteralComponent : MonoBehaviour
    {
        public LiteralData Literal = null;

        public void Reset()
        {
            Literal = new LiteralData(this);
        }
        public void OnEnable()
        {
            Literal.OnInit();
        }
        public void OnDestroy()
        {
            Literal.OnDispose();
        }

#if UNITY_EDITOR
        [ContextMenu("Test Compile")]
        private void TestCompile()
        {
            Literal.TestCompile();
        }
        [ContextMenu("Test Purge")]
        private void TestPurge()
        {
            DelegateManager.Purge();
        }
#endif
    }
}